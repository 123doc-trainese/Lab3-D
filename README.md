<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

# Bài tập Trainee Colombo 2022

## Crawl Data

Tìm hiểu về crawl data, vì sao cần crawl data?

Thực hiện bởi [Phạm Tuấn Anh](https://github.com/TuanAnh0907)

## Liên kết

+ Command : 

    [CrawlerRun](https://gitlab.com/123doc-trainese/Lab3-D/-/blob/Crawldata/app/Console/Commands/CrawlerRun.php)
    
         Khởi tạo trình thu thập thông tin: dạng terminal
    
         Được khởi tạo và chạy liên tục tùy theo cách cấu hình của lập trình

    [CreateJob](https://gitlab.com/123doc-trainese/Lab3-D/-/blob/main/app/Console/Commands/CreateJob.php)
    
          Khởi tạo queue job xử lý quá trình crawl bất đồng bộ 


+ Queue : 

    [CrawlerCacheQueue](https://gitlab.com/123doc-trainese/Lab3-D/-/blob/Crawldata/app/Queue/CrawlerCacheQueue.php)

          Xử lý và điều khiển lưu trữ url thu thập được


+ Model : 
    [CrawlerQueue](https://gitlab.com/123doc-trainese/Lab3-D/-/blob/Crawldata/app/Models/CrawlerQueue.php)

          Model hoạt động như một queue được lưu trên database


+ Observe : 
    [ConsoleObserver](https://gitlab.com/123doc-trainese/Lab3-D/-/blob/Crawldata/app/Observers/ConsoleObserver.php)

           Trình quan sát ( Nơi xử lý thu thập thông tin khi có url được lọc thỏa mãn yêu cầu)


+ Profile : 
   [CustomCrawlProfile](https://gitlab.com/123doc-trainese/Lab3-D/-/blob/main/app/Profile/CustomCrawlProfile.php)

           Lọc các link theo yêu cầu ( Loại bỏ các link quảng cáo, link ngoài không liên quan, ...)

## Hướng dẫn cài đặt test/sử dụng nếu có

Cài đặt một số phần mền hỗ trợ 
+ [Supervisor](https://laravel.com/docs/9.x/queues#supervisor-configuration)  
    
             Giúp queue (hàng đợi) được chạy liên tục, 
 
    Để cài đặt supervisor vui lòng chạy lệnh : 
           
             sudo apt-get install supervisor

    Vào thư mục **/etc/supervisor/conf.d**  tạo file **.conf**. 1 File config cơ bản có cấu trúc như sau: 

            [program:laravel-worker] # mình thường để trùng với tên file cho dễ nhớ :)
            process_name=%(program_name)s_%(process_num)02d
            directory=/var/www/Lab3-D # mọi người tự thay cho phù hợp nhé
            command=php artisan queue:work --sleep=1 --tries=1
            autostart=true
            autorestart=true
            user=root  # user thực hiện command
            numprocs=2
            redirect_stderr=true
            # Nếu có lỗi sẽ được log vào file này
            stdout_logfile=/var/www/Lab3-D/storage/logs/laravel.log


    [**program: tên-bất-kì**]: tên program. Mọi người có thể đặt tên bất kỳ gì.

    [**directory**]: đường dẫn tới program cần run.

    [**command**]: program muốn run.

    [**autostart**]: quy định tiến trình có tự chạy ngay khi Supervisor được khởi động hay ko. Thường thì Supervisor được chạy khi boot, nên việc quy định tiến trình có tự chạy hay không sẽ phụ thuộc vào boot.

    [**autorestart**]: quy định tiến trình có tự khởi động ngay sau khi nó bị tắt vì lý do nào đó (đã chạy xong, bị lỗi, bị timeout…) hay ko.

    [**user**]: user dùng để thực thi tiến trình.

    [**numprocs**]: số lượng tiến trình mà Supervisor sẽ chạy. Ví dụ để gửi mail, ta để numprocs=4, nghĩa là 4 tiến trình cùng chạy task gửi mail.

    [**redirect_stderr**]: nếu là true thì stderr sẽ bị redirect để xuất vào file chỉ định ở stdout_logfile

    [**stdout_logfile**]: đường dẫn tới file log của mình. File này ko cần tạo trước vì supervisor sẽ tự tạo cho mình 😁


   Sau khi cấu hình xong ta chạy lệnh sau để restart lại supervisor 
   
        sudo service supervisor restart

+ [Cron](https://laravel.com/docs/9.x/scheduling#running-the-scheduler)
    
        Đây là cách giúp các tác vụ đã lập lịch của bạn chạy trên 
        máy chủ mà không cần gọi lệnh schedule:run trong command

    Cài đặt dùng lệnh sau : 
    
        sudo apt-get install cron

    Kích hoạt cho phép khởi động dịch vụ ‘crond‘ khi vừa khởi động hệ thống.

        sudo update-rc.d crond enable

    Để chỉnh sửa thêm/xoá một task cron mới hoặc cũ thì ta sử dụng lệnh ‘crontab -e‘ để mở trình editor chỉnh sửa file crontab.
        
        crontab -e

    Dán mã sau bên dưới và lưu lại. Đừng quên thay thế đường dẫn chính xác để có thể chạy artisan command trong ứng dụng Laravel

        * * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1


- Ngoài ra có thể sử dụng command khởi tạo trình thu thập dữ liệu mà không phải chờ đợi :))

  ####   `Cú pháp` : **php artisan crawl { url }**

+ Trong project này Admin panel sử dụng backpack, cách tạo ra các [Operations](https://backpackforlaravel.com/docs/5.x/crud-operations) tại đây

## Kiến thức nắm được

- Cách tạo command trong laravel cùng thực thi lệnh 


- Cách tạo một trình thu thập thông tin crawl với Spatie Crawl


- Nắm được luồng hoạt động cơ bản 


- Tích hợp lưu trữ dữ liệu thu thập và hiển thị trên giao diện

## Todo

Ý tưởng có thể áp dụng thêm với bài tập, phát triển thêm tính năng, mở rộng trường hợp sử dụng, ...

## Credit

- Danh sách các repo, phần mềm đã sử dụng khi thực hiện bài tập

    - Laravel Command [here](https://laravel.com/docs/9.x/artisan)

    - Spatie Crawler [here](https://github.com/spatie/crawler)

    - Dom Crawler [here](https://symfony.com/doc/current/components/dom_crawler.html)

