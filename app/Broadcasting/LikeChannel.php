<?php

namespace App\Broadcasting;

use App\Models\User;
use Laravelista\Comments\Comment;

class LikeChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param \App\Models\User $user
     * @return array|bool
     */
    public function join(User $user, Comment $comment)
    {
        return $user->id === $comment->commenter_id;
    }
}
