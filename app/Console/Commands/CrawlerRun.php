<?php

namespace App\Console\Commands;

use App\Observers\ConsoleObserver;
use App\Profile\CustomCrawlProfile;
use App\Queue\CrawlerCacheQueue;
use GuzzleHttp\RequestOptions;
use Illuminate\Console\Command;
use Spatie\Crawler\Crawler;

class CrawlerRun extends Command
{
    public int $total_crawled = 0;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl {site}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {   
        $queue = null;
        $site = $this->argument('site');

        if (is_null($queue)) {
            $this->info('Preparing a new crawler queue');

            $queue = new CrawlerCacheQueue(86400); // one day
        }
        // Crawler
        $this->info('Start crawling');

        if ($queue->hasPendingUrls()) {
            $site = (string)$queue->getPendingUrl()->url;
        }

        Crawler::create([
            RequestOptions::ALLOW_REDIRECTS => true,
            RequestOptions::TIMEOUT => 60,
            RequestOptions::CONNECT_TIMEOUT => 60,
        ])
            ->setParseableMimeTypes(['text/html', 'text/plain'])
            ->addCrawlObserver(new ConsoleObserver($this))
            ->setConcurrency(30)
            ->setCurrentCrawlLimit(1000)
            ->setTotalCrawlLimit(5000)
            ->setCrawlQueue($queue)
            ->setCrawlProfile(new CustomCrawlProfile($site))
            ->setDelayBetweenRequests(150)
            ->startCrawling($site);

        $this->alert("Crawled {$this->total_crawled} items");

        if ($queue->hasPendingUrls()) {
            $this->alert('Has URLs left');
        } else {
            $this->info('Has no URLs left');
        }
        return 0;

    }
}
