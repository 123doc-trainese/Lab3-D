<?php

namespace App\Console\Commands;

use App\Jobs\CrawlData;
use App\Models\CrawlSite;
use Illuminate\Console\Command;

class CreateJob extends Command
{
    public int $total_crawled = 0;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    protected $signature = 'create:job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $arr_url = CrawlSite::where('status', CrawlSite::STATUS['PENDING'])->get();
        // Create Jobs
        foreach ($arr_url as $value) {
            CrawlData::dispatch($value->url);
        }
        return 0;
    }
}
