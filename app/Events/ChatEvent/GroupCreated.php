<?php

namespace App\Events\ChatEvent;

use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GroupCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $user;
    private $user_id;
    private $group;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $user_id, $group)
    {
        $this->user = $user;
        $this->user_id = $user_id;
        $this->group = $group;
    }

    /**
     * Get the channels the event should broadcast on.     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('my-channel-' . $this->user_id);
    }

    public function broadcastAs()
    {
        return 'add-group-chat-event';
    }

    public function broadcastWith()
    {
        return [
            'user' => $this->user,
            'group' => $this->group,
            'time' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
        ];
    }
}
