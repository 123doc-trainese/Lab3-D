<?php

namespace App\Events\ChatEvent;

use App\Models\Conversation;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class NewMessage implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $conversation;
    private $user_id;
    private $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Conversation $conversation, $user_id)
    {
        $this->conversation = $conversation;
        $this->user_id = $user_id;
        $this->user = Auth::user();
    }

    public function broadcastOn(): Channel
    {
        return new Channel('my-channel-' . $this->user_id);
    }

    public function broadcastAs()
    {
        return 'new-message';
    }

    public function broadcastWith()
    {
        return [
            'message' => $this->conversation->message,
            'group_id' => $this->conversation->group_id,
            'user' => $this->user,
            'time' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
        ];
    }
}
