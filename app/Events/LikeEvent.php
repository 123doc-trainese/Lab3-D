<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class LikeEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user, $commenter_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\App\Models\User $user, $commenter_id)
    {
        $this->user = $user;
        $this->commenter_id = $commenter_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel
     */
    public function broadcastOn()
    {
        return new Channel('my-channel-' . $this->commenter_id);
    }

    public function broadcastAs()
    {
        return 'like-event';
    }

    public function broadcastWith()
    {
        return [
            'user' => $this->user,
            'time' => \Illuminate\Support\Carbon::now()->toDateTimeString(),
        ];
    }
}
