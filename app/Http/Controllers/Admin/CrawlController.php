<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CrawlSite;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Prologue\Alerts\Facades\Alert;

class CrawlController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(backpack_middleware());
    }

    public function runcrawl($id)
    {

        DB::table('crawl_sites')->where('id', $id)->limit(1)->update([
            'status' => CrawlSite::STATUS['PENDING'],
            'updated_at' => Carbon::now()
        ]);

        return redirect()->back();
    }

    public function stopcrawl($id)
    {
        DB::table('crawl_sites')->where('id', $id)->limit(1)->update([
            'status' => CrawlSite::STATUS['CRAWED'],
            'updated_at' => Carbon::now()

        ]);

        return redirect()->back();
    }

    public function setTime(Request $request)
    {
        DB::table('frequences')->delete();

        $string = $request->check ? '*/' : '';

        $num = $request->check ? '*' : '0';

        $data = [];

        if ($request->minute) {
            $data['minute'] = $string . $request->minute;
        } else {
            $data['minute'] = $num;
        }

        if ($request->hour) {
            $data['hour'] = $string . $request->hour;
        } else {
            $data['hour'] = $num;
        }

        if ($request->day) {
            $data['day'] = $string . $request->day;
        } else {
            $data['day'] = '*';
        }

        if ($request->month) {
            $data['month'] = implode(',', $request->month);
        } else {
            $data['month'] = '*';
        }

        if ($request->day_of_week) {
            $data['day_of_week'] = implode(',', $request->day_of_week);
        } else {
            $data['day_of_week'] = '*';
        }

        DB::table('frequences')->insert($data);

        // show a success message
        Alert::success('Setup Time crawl successfully : '. implode(' ', $data))->flash();

        return redirect(backpack_url('crawl-site'));
    }


}
