<?php

namespace App\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait RunOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupRunRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/run', [
            'as' => $routeName . '.run',
            'uses' => $controller . '@run',
            'operation' => 'run',
        ]);
    }

    /**
     * Add the default settings, buttons, etc that this operation needs.
     */
    protected function setupRunDefaults()
    {
        $this->crud->allowAccess('run');

        $this->crud->operation('run', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            // $this->crud->addButton('top', 'run', 'view', 'crud::buttons.run');
            $this->crud->addButton('line', 'run', 'view', 'crud::buttons.runcrawl');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function run()
    {
        $this->crud->hasAccessOrFail('run');
//        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? 'run ' . $this->crud->entity_name;
//        // load the view
        return view("crud::operations.run", $this->data);
    }

}
