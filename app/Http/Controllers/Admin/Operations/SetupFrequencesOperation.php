<?php

namespace App\Http\Controllers\Admin\Operations;

use Illuminate\Support\Facades\Route;

trait SetupFrequencesOperation
{
    /**
     * Define which routes are needed for this operation.
     *
     * @param string $segment Name of the current entity (singular). Used as first URL segment.
     * @param string $routeName Prefix of the route name.
     * @param string $controller Name of the current CrudController.
     */
    protected function setupSetupFrequencesRoutes($segment, $routeName, $controller)
    {
        Route::get($segment . '/setupfrequences', [
            'as' => $routeName . '.setupfrequences',
            'uses' => $controller . '@setupfrequences',
            'operation' => 'setupfrequences',
        ]);
    }

    /**
     * Add the default settings, buttons, etc. that this operation needs.
     */
    protected function setupSetupFrequencesDefaults()
    {
        $this->crud->allowAccess('setupfrequences');

        $this->crud->operation('setupfrequences', function () {
            $this->crud->loadDefaultOperationSettingsFromConfig();
        });

        $this->crud->operation('list', function () {
            $this->crud->addButton('top', 'setupfrequences', 'view', 'crud::buttons.setup');
//            $this->crud->addButton('line', 'setupfrequences', 'view', 'crud::buttons.setup');
        });
    }

    /**
     * Show the view for performing the operation.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function setupfrequences()
    {
        $this->crud->hasAccessOrFail('setupfrequences');
        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['title'] = $this->crud->getTitle() ?? 'setupfrequences ' . $this->crud->entity_name;
        // load the view
        return view("crud::operations.frequences_form", $this->data);
    }
}
