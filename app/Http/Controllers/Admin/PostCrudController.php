<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PostRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PostCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PostCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Post::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/post');
        CRUD::setEntityNameStrings('post', 'posts');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('id');
        CRUD::column('title');
        // CRUD::column('description');
        CRUD::column('view_counts');

        CRUD::addColumn([
            'name'      => 'image',
            'label'     => 'Image',
            'type'      => 'image',
            // 'prefix' => 'public/uploads/',
        ]);
        CRUD::column('highlight_post')->type('check');
        CRUD::column('top_story')->type('check');
        CRUD::column('quick_bite')->type('check');
        CRUD::column('feature_post')->type('check');
        CRUD::column('new_post')->type('check');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    protected function setupShowOperation()
    {
        CRUD::column('id');
        CRUD::column('title');
        CRUD::addColumn([
            'name'      => 'description',
            'label'     => 'Description',
            'type'      => 'custom_html',
            'value'    => (function ($post) {
                return $post->description;
            }),
            // 'escaped' => true ,
        ]);
        CRUD::addColumn([
            'name'      => 'content',
            'label'     => 'Content',
            'type'      => 'custom_html',
            'value'    => (function ($post) {
                return $post->content;
            }),
            // 'escaped' => true ,
        ]);

        CRUD::addColumn([
            'name'      => 'image',
            'label'     => 'Image',
            'type'      => 'image',
            // 'prefix' => 'public/uploads/',
        ]);

        CRUD::addColumn([
            'name'      => 'category_id',
            'label'     => "Category",
            'type'      => 'text',
            'value'    => (function ($post) {
                return $post->category_id;
            }),
        ]);
        CRUD::column('view_counts');
        CRUD::column('highlight_post')->type('check');
        CRUD::column('top_story')->type('check');
        CRUD::column('quick_bite')->type('check');
        CRUD::column('feature_post')->type('check');
        CRUD::column('new_post')->type('check');
        CRUD::column('created_at');
        CRUD::column('updated_at');
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PostRequest::class);
        CRUD::field('title');
        CRUD::field('description');
        CRUD::addField([
            'name' => 'content',
            'type' => 'summernote',
            'label' => "Content description",
            'options' => [
                'minheight: 300'
            ],
        ]);

        CRUD::field('image');
        // CRUD::addField([
        //     'name'      => 'image',
        //     'label'     => 'Image',
        //     'type'      => 'upload',
        //     'upload'    => true,
        //     'disk'      => 'uploads',
        // ]);

        CRUD::field('description');

        CRUD::addField([
            'label'     => "Category",
            'type'      => 'select',
            'name'      => 'category_id',
            'entity'    => 'category',
            'attribute' => 'name',
            'model'     => "App\Models\Category",
            'data_source' => url('admin/category'),
            'options'   => (function ($query) {
                return $query->orderBy('name', 'ASC')->get();
            }),
        ]);

        CRUD::field('highlight_post')->type('checkbox');
        CRUD::field('top_story')->type('checkbox');
        CRUD::field('quick_bite')->type('checkbox');
        CRUD::field('feature_post')->type('checkbox');
        CRUD::field('new_post')->type('checkbox');

        CRUD::field('hidden');


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }
    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
