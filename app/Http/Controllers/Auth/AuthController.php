<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    //
    public function login()
    {
        return view('auth.login');
    }

    function checkLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $arr = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if (Auth::attempt($arr)) {

            $user = DB::table('users')->where('email', $request->email)->first();

            if ($user->email_verified_at != NULL) {
                # code...
                if ($request->has('remember')) {
                    # code...
                    Cookie::queue('email', $request->email, 1440);
                    Cookie::queue('password', $request->password, 1440);
                }
                return redirect(backpack_url('dashboard'));
            }
            return redirect()->route('web.waitting')->with('errors', 'You not confirm email !');
        }
        return redirect()->back()->with('errors', 'Failed Login');
    }

    function logout()
    {
        Auth::logout();
        return redirect()->route('web.home');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function store(Request $request)
    {
        # code...
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6|max:32',
            'confirm' => 'same:password',
        ]);

        $token = Str::random(64);

        $data =  [
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'is_admin' => 0,
            'token' => $token
        ];

        User::create($data);

        $user = User::where([
            'email' => $request->email
        ])->first();

        SendEmail::dispatch($token, $request->name, $request->email, 'auth.email.createAcount',
            'Create Account by Email');

        Auth::login($user);

        return redirect()->route('web.waitting')->with('message', 'Create account success, confirm email !');
    }

    public function profile()
    {
        $categories = Category::take(10)->get();
        return view('web.editprofile', compact('categories'));
    }

    public function updateProfile(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);

        $user = User::find(Auth::user()->id);

        $data = [
            'name' => $request->name,
        ];

        if (Hash::check($request->old_password, $user->password) == true) {
            # code...
            if ($request->new_password) {
                $this->validate($request, [
                    'new_password' => 'required|min:6|max:32',
                    'confirm' => 'same:new_password',
                ]);
                $data['password'] = bcrypt($request->new_password);
            }

            // return response()->json($data);

            $user->update($data);

            return redirect()->route('web.profile')->with('message', 'Updated successfully');
        } else {
            return redirect()->route('web.profile')->with('errors', 'Updated Failed! Password check false ');
        }
    }
}
