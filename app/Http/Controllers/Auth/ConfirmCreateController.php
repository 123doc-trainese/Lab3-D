<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\User;

class ConfirmCreateController extends Controller
{
    //
    public function waitting()
    {
        return view('auth.waittingConfirm');
    }

    public function resendEmailConfirm(Request $request)
    {
        $token = Str::random(64);

        $data = [
            'email' => $request->email,
            'token' => $token
        ];

        $user = User::find(Auth::user()->id);

        SendEmail::dispatch($token, $user->name, $request->email, 'auth.email.createAcount',
            'Create Account by Email');

        $user->update($data);

        return redirect()->back()->with('message', 'We send new email to your gmail, please confirm now');
    }

    public function submitConfirmForm($token)
    {
        $user = User::where([
            'token' => $token
        ])->first();

        if (!$user) {
            return back()->withInput()->with('errors', 'Invalid token!');
        }

        $user->update([
            'email_verified_at' => Carbon::now(),
            'token' => Null
        ]);

        Auth::login($user);

        return redirect()->route('web.home')->with('message', 'Email verification successful');;
    }
}
