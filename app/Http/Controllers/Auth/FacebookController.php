<?php

namespace App\Http\Controllers\Auth;

use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\User;
class FacebookController extends Controller
{
    //
    public function loginUsingFacebook()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callbackFromFacebook()
    {
        $user = Socialite::driver('facebook')->user();

        $token = Str::random(64);

        $saveUser = User::updateOrCreate([
            'facebook_id' => $user->getId(),
        ], [
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'password' => bcrypt($user->getName() . '@' . $user->getId()),
            'is_admin' => 0,
            'email_verified_at' => Carbon::now(),
            'token' => $token
        ]);

        Auth::loginUsingId($saveUser->id);

        return redirect()->route('web.home');
    }
}
