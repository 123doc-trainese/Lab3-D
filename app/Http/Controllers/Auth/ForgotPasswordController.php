<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    //
    public function showForgetPasswordForm()
    {
        return view('auth.forgetPassword');
    }

    public function submitForgetPasswordForm(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
        ]);

        $token = Str::random(64);

        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        $user = User::where([
            'email' => $request->email,
        ])->first();

        SendEmail::dispatch($token, $user->name, $request->email, 'auth.email.forgetPassword',
            'Reset Password For Account');

        return back()->with('message', 'We have e-mailed your password reset link!');
    }

    public function showResetPasswordForm($token)
    {
        $updatePassword = DB::table('password_resets')->where(['token' => $token])->first();

        if ($updatePassword) {
            return view('auth.forgetPasswordLink', ['token' => $token, 'email' => $updatePassword->email]);
        }

        return abort(404);
    }

    public function submitResetPasswordForm(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|min:6|max:32',
            'password_confirmation' => 'same:password',
        ]);

        $updatePassword = DB::table('password_resets')->where([
            'email' => $request->email,
            'token' => $request->token
        ])->first();

        if (!$updatePassword) {
            return back()->withInput()->with('errors', 'Invalid token!');
        }

        User::where('email', $request->email)->update([
            'password' => bcrypt($request->password)
        ]);

        $user = User::where([
            'email' => $request->email,
        ])->first();

        DB::table('password_resets')->where(['email' => $request->email])->delete();

        Auth::login($user);

        return  redirect()->route('web.home')->with('message', 'Your password has been changed!');
    }
}
