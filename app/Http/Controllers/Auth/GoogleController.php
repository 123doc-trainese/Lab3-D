<?php

namespace App\Http\Controllers\Auth;

use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\User;

class GoogleController extends Controller
{
    //
    public function loginWithGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function callbackFromGoogle()
    {

        $user = Socialite::driver('google')->user();

        $is_user = User::where('email', $user->getEmail())->first();

        if (!$is_user) {
            $token = Str::random(64);

            $saveUser = User::updateOrCreate([
                'google_id' => $user->getId(),
            ], [
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'password' => bcrypt($user->getName() . '@' . $user->getId()),
                'is_admin' => 0,
                'email_verified_at' => Carbon::now(),
                'token' => $token
            ]);
        } else {
            $saveUser = User::where('email',  $user->getEmail())->update([
                'google_id' => $user->getId(),
            ]);
//            $saveUser = User::where('email', $user->getEmail())->first();
        }

        Auth::loginUsingId($saveUser->id);

        return redirect()->route('web.home');
    }
}
