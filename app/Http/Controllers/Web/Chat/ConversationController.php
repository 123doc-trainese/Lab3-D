<?php

namespace App\Http\Controllers\Web\Chat;

use App\Events\ChatEvent\NewMessage;
use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Models\Group;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ConversationController extends Controller
{
    public function store(Request $request)
    {
        $list_user = DB::table('group_user')->where('group_id', $request->group_id)->get();

        foreach ($list_user as $user) {
            if (auth()->id() != $user->user_id) {

                $conversation = Conversation::create([
                    'message' => $request->message,
                    'group_id' => $request->group_id,
                    'user_id' => auth()->id(),
                    'send_user' => $user->user_id,
                    'status' => Conversation::STATUS['MESSAGE'],
                ]);

                broadcast(new NewMessage($conversation, $user->user_id))->toOthers();
            }
        }

        return $conversation;
    }


    public function getConversations(Request $request)
    {
        $con = DB::table('conversations')->join('users', 'conversations.user_id', '=', 'users.id')->where('group_id',
            $request->id)->orderBy('conversations.created_at', 'asc')->get();

        $group = Group::where('id', $request->id)->first();

        Conversation::where('group_id', $request->id)
            ->where('send_user', \auth()->id())
            ->where('read_at', null)
            ->update([
                'read_at' => Carbon::now()->toDateTimeString()
            ]);

        return compact('con', 'group');
    }

    public function readAt(Request $request)
    {
        $conversation = Conversation::where('send_user', Auth::id())
            ->where('read_at', null)
            ->where('group_id', $request->id)
            ->update([
                'read_at' => Carbon::now(),
            ]);

        return $conversation;
    }
}
