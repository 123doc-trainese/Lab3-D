<?php

namespace App\Http\Controllers\Web\Chat;

use App\Events\ChatEvent\GroupCreated;
use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Models\Group;
use App\Models\User;
use App\Notifications\AddGroupNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class GroupController extends Controller
{
    public function store(Request $request)
    {
        $group = Group::create([
            'id' => Str::uuid()->toString(),
            'name' => $request->name,
        ]);

        // Insert user creat group in db
        DB::table('group_user')->insert([
            'group_id' => $group->id,
            'user_id' => auth()->id(),
            'status' => User::STATUS['Join'],
        ]);

        return $group;
    }

    public function search(Request $request)
    {
        return User::where('name', 'like', "%{$request->name}%")->take(12)->get();
    }

    public function saveUser(Request $request)
    {
        $check = DB::table('group_user')->where([
            'user_id' => $request->id_user,
            'group_id' => $request->id_group,
        ])->first();

        if (!$check) {
            $save = DB::table('group_user')->insert([
                'user_id' => $request->id_user,
                'group_id' => $request->id_group,
                'status' => User::STATUS['Join'],
            ]);

            if ($save) {
                $user = User::where('id', $request->id_user)->first();
                $group = Group::where('id', $request->id_group)->first();

                Conversation::create([
                    'message' => Auth::user()->name . ' đã thêm ' . $user->name . ' vào nhóm chat',
                    'group_id' => $request->id_group,
                    'user_id' => auth()->id(),
                    'send_user' => $user->id,
                    'status' => Conversation::STATUS['ACCTION'],
                ]);

                Notification::send($user, new AddGroupNotification(Auth::user(), $group));
                broadcast(new GroupCreated(Auth::user(), $request->id_user, $group))->toOthers();

                return [
                    'user' => $user,
                    'message' => 'Mời thành công',
                ];
            }
        }

        return $message = 'Mời không thành công';
    }

    public function getListGroup()
    {
        $group = DB::table('groups')->join('group_user', 'groups.id', '=',
            'group_user.group_id')->where('group_user.user_id', auth()->id())->where('group_user.status',
            User::STATUS['Join'])->orderBy('groups.created_at', 'desc')->get();

        $result = [];
        foreach ($group as $group){
            $result[] = [
                'group' => $group,
                'count' => Conversation::countNewMessage($group->group_id),
            ];
        }
        return $result;
    }

    public function getUsers(Request $request)
    {
        $users = DB::table('group_user')->join('users', 'group_user.user_id', '=',
            'users.id')->where('group_user.group_id',
            $request->id)->get();

        return $users;
    }

    public function leaveGroup($id)
    {
        DB::table('group_user')->where('group_id', $id)->where('user_id', Auth::id())->delete();

        Conversation::create([
            'message' => Auth::user()->name . ' đã thêm rời nhóm chat',
            'group_id' => $id,
            'user_id' => auth()->id(),
            'status' => Conversation::STATUS['ACCTION'],
        ]);

        return redirect()->back();
    }
}
