<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    //
    public function home()
    {

        $categories = Category::take(10)->get();

        $highlight_post = Post::where('highlight_post', 1)->where('hidden', 1)->orderBy('created_at', 'desc')->take(1)->get();

        $top_stories = Post::where('top_story', 1)->where('hidden', 1)->take(7)->get();

        $quick_bites = Post::where('quick_bite', 1)->where('hidden', 1)->take(1)->inRandomOrder()->get();

        $feature_posts = Post::where('feature_post', 1)->where('hidden', 1)->take(5)->get();

        $worldnew_posts = Post::where('category_id', 3)->where('hidden', 1)->take(2)->get();

        $entertaiment_posts = Post::where('category_id', 5)->where('hidden', 1)->take(8)->get();

        $sport_posts = Post::where('category_id', 1)->where('hidden', 1)->take(2)->get();

        $new_posts = Post::where('new_post', 1)->where('hidden', 1)->orderBy('created_at', 'desc')->take(5)->get();

        $most_views = Post::where('hidden', 1)->where('hidden', 1)->orderBy('view_counts', 'desc')->take(10)->get();

        $most_cmts = Post::where('hidden', 1)->where('hidden', 1)->orderBy('updated_at', 'desc')->take(4)->get();

        return view('web.home', compact(
            'highlight_post',
            'top_stories',
            'quick_bites',
            'feature_posts',
            'new_posts',
            'categories',
            'worldnew_posts',
            'entertaiment_posts',
            'sport_posts',
            'most_views',
            'most_cmts'
        ));
    }

    public function detail($slug)
    {
        $categories = Category::take(10)->get();

        $all_category = Category::get();

        $post = Post::where('slug', $slug)->where('hidden', 1)->first();

        DB::table('posts')
            ->where('slug', $slug)
            ->update(['view_counts' => $post->view_counts + 10]);

        $new_posts = Post::where('new_post', 1)->where('hidden', 1)->orderBy('created_at', 'desc')->take(4)->get();

        $relate = Post::where('category_id', $post->category_id)->where('slug', '<>', $slug)->where('hidden',
            1)->take(3)->inRandomOrder()->get();

        $most_views = Post::where('hidden', 1)->where('slug', '<>', $slug)->orderBy('view_counts',
            'desc')->take(4)->get();

        return view('web.detail', compact('post', 'new_posts', 'relate', 'most_views', 'categories', 'all_category'));
    }

    public function category()
    {
        $categories = Category::take(10)->get();

        $all_category = Category::get();

        $posts = Post::where('hidden', 1)->inRandomOrder()->orderBy('created_at', 'desc')->paginate(10);

        return view('web.categories', compact('categories', 'posts', 'all_category'));
    }

    function categorySlug($slug)
    {
        $categories = Category::take(10)->get();

        $all_category = Category::get();

        $category = Category::where('slug', $slug)->first();

        $posts = Post::where('category_id', $category->id)->orderBy('created_at', 'desc')->paginate(10);

        return view('web.categories', compact('categories', 'posts', 'all_category'));
    }
}
