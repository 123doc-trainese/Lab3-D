<?php

namespace App\Http\Controllers\Web\Like;

use App\Events\LikeEvent;
use App\Http\Controllers\Controller;
use App\Models\Like;
use App\Models\User;
use App\Notifications\UserLikeNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Laravelista\Comments\Comment;

class LikeController extends Controller
{
    public function like(Request $request)
    {
        $check = Like::where([
            'user_id' => Auth::user()->id,
            'comment_id' => $request->comment_id
        ])->count();

        if (!$check) {
            $liked = Like::create([
                'user_id' => Auth::user()->id,
                'liker_type ' => $request->liker_type,
                'comment_id' => $request->comment_id
            ]); 
            if ($liked) {
                $comment = Comment::where('id', $request->comment_id)->first();
                $user = User::where('id', $comment->commenter_id)->first();

                if (Auth::user()->id != $user->id) {
                    Notification::send($user, new UserLikeNotification(Auth::user()));
                    broadcast(new LikeEvent(Auth::user(), $user->id))->toOthers();
                }
            }
        }
        return redirect()->back();
    }

    public function dislike(Request $request)
    {
        Like::where([
            'user_id' => Auth::user()->id,
            'comment_id' => $request->comment_id
        ])->delete();
        return redirect()->back();
    }

    public function countLike($comment_id)
    {
        $value = Like::where([
            'comment_id' => $comment_id
        ])->count();

        return $value;
    }

    public function getLiker($comment_id)
    {
        $likers = DB::table('likes')->join('users', 'likes.user_id', '=', 'users.id')->where('comment_id',
            $comment_id)->get();
        return $likers;
    }
}
