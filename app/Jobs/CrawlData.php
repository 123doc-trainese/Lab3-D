<?php

namespace App\Jobs;

use App\Models\CrawlSite;
use App\Observers\QueueCrawlObserver;
use App\Profile\CustomCrawlProfile;
use App\Queue\CrawlerCacheQueue;
use GuzzleHttp\RequestOptions;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Crawler\Crawler;


class CrawlData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $url;

    /**
     * Số lần job sẽ thử thực hiện lại
     *
     * @var int
     */
    public $tries = 3;

    public $timeout = 180;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url)
    {
        $this->url = $url;
        Log::channel('crawl_log')->info("Create job crawl : {$url}");
    }

    /**
     * Execute the job.
     *
     * @return int
     */
    public function handle()
    {
        $queue = null;
        $site =  $this->url;
        $total_crawled = 0;
        //run
        DB::table('crawl_sites')->where('url', $site)->update(['status' => CrawlSite::STATUS['CRAWLING']]);

        if (is_null($queue)) {
            $queue = new CrawlerCacheQueue(86400); // one day
        }

        if ($queue->hasPendingUrls()) {
            $site = (string)$queue->getPendingUrl()->url;
        }
        // Crawler
        Crawler::create([
            RequestOptions::ALLOW_REDIRECTS => true,
            RequestOptions::TIMEOUT => 60,
            RequestOptions::CONNECT_TIMEOUT => 60,
        ])
            ->setParseableMimeTypes(['text/html', 'text/plain'])
            ->addCrawlObserver(new QueueCrawlObserver())
            ->setConcurrency(30)
            ->setCrawlQueue($queue)
            ->setCurrentCrawlLimit(30)
            ->setTotalCrawlLimit(200)
            ->setCrawlProfile(new CustomCrawlProfile($site))
            ->setDelayBetweenRequests(200)
            ->startCrawling($site);

        DB::table('crawl_sites')->where('status', CrawlSite::STATUS['CRAWLING'])->update(['status' => CrawlSite::STATUS['PENDING']]);
        Log::channel('crawl_log')->warning("Stop Job ");
        return 0;
    }
}
