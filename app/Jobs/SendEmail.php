<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $token;
    private $name;
    private $mail;
    private $content;
    private $subject;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($token, $name, $mail, $content, $subject)
    {
        $this->token = $token;
        $this->name = $name;
        $this->mail = $mail;
        $this->content = $content;
        $this->subject = $subject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send($this->content, ['token' => $this->token, 'name' => $this->name],
            function ($message) {
                $message->to($this->mail);
                $message->subject($this->subject);
            });
    }
}
