<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use App\Models\User;

class ForgetPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $token)
    {
        //
        $this->user = $user;
        $this->token = $token;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        // return $this->markdown('auth.email.forgetPassword',[
        //     'token' => $this->token, 'name' => $this->user->name
        // ])
        // ->subject('Reset Password For Account');

        return $this->view('auth.email.forgetPassword',[
            'token' => $this->token, 'name' => $this->user->name
        ])
        ->subject('Reset Password For Account');
    }
}
