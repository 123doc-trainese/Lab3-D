<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Conversation extends Model
{
    use HasFactory;

    protected $table = 'conversations';

    protected $fillable = [
        'message',
        'user_id',
        'group_id',
        'send_user',
        'read_at',
        'status',
    ];

    const STATUS = [
        'MESSAGE' => 0,
        'ACCTION' => 1,
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::updating(function ($conversation) {
            return $conversation->count();
        });
    }

    public static function countNewMessage($group_id): int
    {
        return Conversation::where('group_id', $group_id)
            ->where('send_user', Auth::id())
            ->where('read_at', null)->count();
    }

    public static function countAllNewMessage(): int
    {
        return Conversation::where('send_user', Auth::id())
            ->where('read_at', null)->count();
    }
}
