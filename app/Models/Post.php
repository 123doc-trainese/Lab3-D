<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Laravelista\Comments\Commentable;


use App\Models\Category;

class Post extends Model
{
    use CrudTrait;
    use Sluggable;
    use Commentable;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'posts';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    protected $fillable = [
        'title',
        'description',
        'content',
        'image',
        'view_counts',
        'user_id',
        'new_post',
        'hidden',
        'slug',
        'category_id',
        'highlight_post',
        'top_story',
        'quick_bite',
        'feature_post',
    ];
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
                'onUpdate' => true,
            ]
        ];
    }

    // public function imageUrl()
    // {
    //     return '/public/uploads/' . $this->image;
    // }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function category()
    {
        return $this->beLongsTo(Category::class, 'category_id', 'id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    // public function setImageAttribute($value)
    // {
    //     $attribute_name = "image";
    //     $disk = "uploads";
    //     $destination_path = "";
    //     $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    // }

    // public static function boot()
    // {
    //     parent::boot();
    //     static::deleted(function ($obj) {
    //         Storage::disk('public')->delete($obj->image);
    //     });
    // }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
