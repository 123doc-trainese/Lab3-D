<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laravelista\Comments\Commenter;
use Spatie\Permission\Traits\HasRoles;

// <------------------------------- this one
// <---------------------- and this one

class User extends Authenticatable implements MustVerifyEmail
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    use HasApiTokens;
    use HasFactory;
    use Notifiable;
    use CrudTrait;
    use HasRoles;
    use Commenter;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */

    const AVATA = 'https://previews.123rf.com/images/triken/triken1608/triken160800029/61320775-male-avatar-profile-picture-default-user-avatar-guest-avatar-simply-human-head-vector-illustration-i.jpg';

    const STATUS = [
        'Add' => 0,
        'Join' => 1,
        'Exit' => 2,
    ];

    protected $fillable = [
        'name',
        'email',
        'password',
        'is_admin',
        'token',
        'facebook_id',
        'google_id',
        'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
