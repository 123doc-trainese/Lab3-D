<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;

class AddGroupNotification extends Notification
{
    use Queueable;

    protected $user;
    protected $group;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $group)
    {
        $this->user = $user;
        $this->group = $group;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user_id' => $this->user->id,
            'username' => $this->user->name,
            'avata' => $this->user->avata,
            'group' => $this->group,
            'time' => Carbon::now()->toDateTimeString(),
            'content' => 'Đã mời bạn tham gia nhóm chat',
        ];
    }
}
