<?php

namespace App\Observers;

use App\Models\Category;
use App\Models\CrawlerQueue;
use App\Models\Post;
use DOMDocument;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlObservers\CrawlObserver;
use Symfony\Component\DomCrawler\Crawler as DomCrawler;


class QueueCrawlObserver extends CrawlObserver
{
    private $content;

    public function __construct()
    {
        $this->content = null;
    }

    /**
     * @param UriInterface $url
     */
    public function willCrawl(UriInterface $url): void
    {
        Log::channel('crawl_log')->info("Search : {$url}");
    }

    /**
     * Called when the crawler has crawled the given url successfully.
     *
     * @param UriInterface $url
     * @param ResponseInterface $response
     * @param UriInterface|null $foundOnUrl
     */
    public function crawled(UriInterface $url, ResponseInterface $response, ?UriInterface $foundOnUrl = null): void
    {
        CrawlerQueue::onlyTrashed()->url($url)->first();

        $doc = new DOMDocument();

        @$doc->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . $response->getBody());

        //# save HTML
        $content = $doc->saveHTML();

        $crawler = new DomCrawler($content);

        $title = $this->crawlData('.title-detail', $crawler);

        switch ($title) {
            case null:
                # code...
                Log::channel('crawl_log')->warning(("Not article : {$url} ({$foundOnUrl})"));
                break;
            default:
                # code...
                // scrape post
                $category_id = $this->crawlCategory('ul.breadcrumb li a', $crawler);

                $title = $this->crawlData('h1.title-detail', $crawler);

                $description = $this->crawlData('p.description', $crawler);

                $created_at = $this->crawlData('span.date', $crawler);

                $image = $this->crawlImage('img.lazy', $crawler);

                $contents = $crawler->filter('article.fck_detail ')->html();
                // filter image
                $contents = preg_replace("/\bposition: absolute;\b/", "", $contents);
                //filter cmt
                $contents = preg_replace("/\b<!--[if IE]>\w*<![endif]-->/", "", $contents);
                //filter padding
                $contents = preg_replace("/\bpadding-bottom: \d+.\d*%;\b/", "padding-bottom: 5%; object-fit: cover; width: 100%; ", $contents);

                $contents = preg_replace("/\bwidth:\d+px;/", "", $contents);

                $contents = preg_replace("/\bwidth:\d+px;/", "", $contents);

                $contents = preg_replace("/src=\"data\S*\"/", "", $contents);

                $contents = str_replace("data-src", "src", $contents);

                $data = [
                    'category_id' => $category_id,
                    'title' => $title,
                    'description' => $description,
                    'image' => $image,
                    'content' => $contents,
                    'created_at' => $created_at,
                    'view_counts' => random_int(1000, 10000),
                    'new_post' => random_int(0, 1),
                    'highlight_post' => random_int(0, 1),
                    'top_story' => random_int(0, 1),
                    'quick_bite' => random_int(0, 1),
                    'feature_post' => random_int(0, 1),
                ];
                if ($image != '') {
                    if (Post::create($data)) {
                        Log::channel('crawl_log')->info("Crawled: {$url}. in ($foundOnUrl) ");
                    } else {
                        Log::channel('crawl_log')->error("Link: {$url}. not image in ($foundOnUrl) ");
                    }
                }
                break;
        }
    }

    protected function crawlCategory(string $type, $crawler)
    {
        $result = $crawler->filter($type)->each(function ($node) {
            return $node->text();
        });

        if (empty($result)) {
            $result[] = "Khác";
        }

        $check = Category::where('name', $result[0])->first();

        if (!$check) {
            $check = Category::create(['name' => $result[0]]);
        }

        return $check->id;
    }

    protected function crawlImage(string $type, $crawler)
    {
        $result = $crawler->filter($type)->each(function ($node) {
            return $node->attr('data-src');
        });

        if (!empty($result)) {
            return $result[0];
        }

        return '';
    }

    protected function crawlData(string $type, $crawler)
    {
        $result = $crawler->filter($type)->each(function ($node) {
            return $node->html();
        });

        if (!empty($result)) {
            return $result[0];
        }

        return '';
    }

    /**
     * Called when the crawler had a problem crawling the given url.
     *
     * @param UriInterface $url
     * @param RequestException $requestException
     * @param UriInterface|null $foundOnUrl
     */
    public function crawlFailed(
        UriInterface $url,
        RequestException $requestException,
        ?UriInterface $foundOnUrl = null
    ): void {
        Log::channel('crawl_log')->error("Crawl Failed : {$url} with {$requestException->getMessage()}");
    }

    /**
     * Called when the crawl has ended.
     */
    public function finishedCrawling(): void
    {
        Log::channel('crawl_log')->notice(" Finished Crawl");
    }
}
