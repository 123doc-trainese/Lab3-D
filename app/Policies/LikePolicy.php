<?php

namespace App\Policies;

use App\Models\Like;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;
use Laravelista\Comments\Comment;

class LikePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function like(User $user, Comment $comment): bool
    {
        $like = Like::where([
            'user_id' => Auth::user()->id,
            'comment_id' => $comment->getKey()
        ])->first();

        return $like ? false : true;
    }

    /**
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function destroy(User $user, Comment $comment): bool
    {
        return $this->like($user, $comment) ? false : true;
    }
}
