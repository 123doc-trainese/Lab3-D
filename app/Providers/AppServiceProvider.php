<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        URL::forceRootUrl(Config::get('app.url'));
//        if (Str::contains(Config::get('app.url'), 'https://')) {
//            URL::forceScheme('https');
//            //use \URL:forceSchema('https') if you use laravel < 5.4
//        }
        Paginator::useTailwind();
    }
}
