<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrawlerQueues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crawler_queues', function (Blueprint $table) {
            $table->id();

            $table->longText('url_hash', 128);
            $table->text('url');

            $table->longText('url_class');

            $table->expires();
            $table->index('expires_at');

            $table->timestamps();
            // @OBS deleted_at = soft delete = processed
            $table->softDeletes()->comment('Means processed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crawler_queues');
    }
}
