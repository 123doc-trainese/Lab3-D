<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrawlSites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crawl_sites', function (Blueprint $table) {
            $table->id();
            $table->string('url')->unique();
            $table->smallInteger('status')->default(\App\Models\CrawlSite::STATUS['NEW']);
            $table->timestamps();
//            $table->softDeletes()->comment('Means processed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crawl_sites');
    }
}
