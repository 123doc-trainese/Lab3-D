<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class Category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->delete();
        DB::table('categories')->insert([
            [ 'id' => 1, 'name' => 'World', 'slug' => 'world'],
            [ 'id' => 2, 'name' => 'Politics', 'slug' => 'politics'],
            [ 'id' => 3, 'name' => 'Business', 'slug' => 'business'],
            [ 'id' => 4, 'name' => 'Opinion', 'slug' => 'opinion'],
            [ 'id' => 5, 'name' => 'Tech', 'slug' => 'tech'],
            [ 'id' => 6, 'name' => 'Science', 'slug' => 'science'],
            [ 'id' => 7, 'name' => 'Heath', 'slug' => 'heath'],
            [ 'id' => 8, 'name' => 'Sport', 'slug' => 'sport'],
            [ 'id' => 9, 'name' => 'Entertaiment', 'slug' => 'entertaiment'],
            [ 'id' => 10, 'name' => 'Travel', 'slug' => 'travel'],
            [ 'id' => 11, 'name' => 'Q-A', 'slug' => 'question-and-answer'],
            [ 'id' => 12, 'name' => 'Car', 'slug' => 'Car'],
            [ 'id' => 13, 'name' => 'Football', 'slug' => 'football'],

        ]);
    }
}
