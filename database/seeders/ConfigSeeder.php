<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->delete();
        DB::table('configs')->insert([
            [
                'id' => 1,
                'name' => 'FREQUENCES_SITE_SCAN',
                'value' => '* * * * *',
                'description' => 'Crawl time loop',
                'group' => 'Crawl Site',
            ],
        ]);
    }
}
