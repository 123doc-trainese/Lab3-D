<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Permistion extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('permissions')->delete();
        DB::table('permissions')->insert([
            [ 'id' => 1, 'name' => 'logs', 'guard_name' => 'backpack'],
            [ 'id' => 2, 'name' => 'backups', 'guard_name' => 'backpack'],
            [ 'id' => 3, 'name' => 'file manager', 'guard_name' => 'backpack'],
            [ 'id' => 4, 'name' => 'manage permissions', 'guard_name' => 'backpack'],
            [ 'id' => 5, 'name' => 'manage roles', 'guard_name' => 'backpack'],
            [ 'id' => 6, 'name' => 'manage users', 'guard_name' => 'backpack'],
            [ 'id' => 7, 'name' => 'manage pages', 'guard_name' => 'backpack'],

        ]);
    }
}
