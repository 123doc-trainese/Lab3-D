</html>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <style type="text/css">
        #outlook a {
            padding: 0;
        }

        body {
            margin: 0;
            padding: 0;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table,
        td {
            border-collapse: collapse;
            mso-table-lspace: 0;
            mso-table-rspace: 0;
        }

        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        p {
            display: block;
            margin: 13px 0;
        }
    </style>
    <style type="text/css">
        @media only screen and (min-width:480px) {
            .mj-column-per-100 {
                width: 100% !important;
                max-width: 100%;
            }
        }
    </style>
    <style type="text/css">
        @media only screen and (max-width:480px) {

        }
    </style>
    <style type="text/css">
        a,
        span,
        td,
        th {
            -webkit-font-smoothing: antialiased !important;
            -moz-osx-font-smoothing: grayscale !important;
        }
    </style>
    <title></title>
</head>

<body style="background-color:#ffffff;">
<div style="background-color:#ffffff;">
    <div style="margin:0 auto;max-width:600px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
            <tr>
                <td style="direction:ltr; font-size:0; padding: 20px 0 0; text-align:center;">
                    <div class="mj-column-per-100 mj-outlook-group-fix"
                         style="font-size:0; text-align:left; direction:ltr; display:inline-block; vertical-align:top; width:100%;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="vertical-align:top;" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" style="font-size:0;padding:10px 25px;word-break:break-word;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                           style="border-collapse:collapse;border-spacing:0;">
                                        <tbody>
                                        <tr>
                                            <td style="width:50px;">
                                                <img alt="image description" height="auto"
                                                     src="https://codedmails.com/images/logo-circle.png"
                                                     style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:14px;"
                                                     width="50"/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="font-size:0; padding:10px 25px; word-break:break-word;">
                                    <div
                                        style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:400;line-height:24px;text-align:left;color:#434245;">
                                        <h1 style="margin: 0; font-size: 24px; line-height: normal; font-weight: bold;">
                                            Welcome to <br/> The New! </h1>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div style="margin:0 auto;max-width:600px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
            <tr>
                <td style="direction:ltr;font-size:0;padding:20px 0;text-align:center;">
                    <div class="mj-column-per-100 mj-outlook-group-fix"
                         style="font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="vertical-align:top;" width="100%">
                            <tbody>
                            <tr>
                                <td align="left" style="font-size:0;padding:10px 25px;word-break:break-word;">
                                    <div
                                        style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:400;line-height:24px;text-align:left;color:#434245;">
                                        Hi {{ $name }},
                                    </div>
                                    <div
                                        style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:400;line-height:24px;text-align:left;color:#434245;">
                                        We have approved your request, and we are really excited to welcome you.
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="font-size:0;padding:10px 25px;word-break:break-word;">
                                    <div
                                        style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:400;line-height:24px;text-align:left;color:#434245;">
                                        Click on the link below to complete your registration and get started!
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" vertical-align="middle"
                                    style="font-size:0;padding:10px 25px;word-break:break-word;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                           style="border-collapse:separate;line-height:100%;">
                                        <tbody>
                                        <tr>
                                            <td align="center" bgcolor="#2e58ff" role="presentation"
                                                style="border:none;border-radius:30px;cursor:auto;mso-padding-alt:10px 25px;background:#2e58ff;"
                                                valign="middle">
                                                <a href="{{ route('confirm.create.account.post', $token) }}" style="display: inline-block; background: #2e58ff; color: #ffffff; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-weight: bold; line-height: 30px; margin: 0; text-decoration: none; text-transform: uppercase; padding: 10px
25px; mso-padding-alt: 0; border-radius: 30px;" target="_blank"> Complete registration </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="font-size:0;padding:10px 25px;word-break:break-word;">
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" role="presentation"
                                           style="float:none;display:inline-table;">
                                        <tbody>
                                        <tr>
                                            <td style="padding:4px;">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                       style="border-radius:3px;width:18px;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="font-size:0;height:18px;vertical-align:middle;width:18px;">
                                                            <a href="#" target="_blank"
                                                               style="color: #2e58ff; text-decoration: none;">
                                                                <img alt="twitter-logo" height="18"
                                                                     src="https://codedmails.com/images/social/black/twitter-logo-transparent-black.png"
                                                                     style="border-radius:3px;display:block;"
                                                                     width="18"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" role="presentation"
                                           style="float:none;display:inline-table;">
                                        <tbody>
                                        <tr>
                                            <td style="padding:4px;">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                       style="border-radius:3px;width:18px;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="font-size:0;height:18px;vertical-align:middle;width:18px;">
                                                            <a href="#" target="_blank"
                                                               style="color: #2e58ff; text-decoration: none;">
                                                                <img alt="facebook-logo" height="18"
                                                                     src="https://codedmails.com/images/social/black/facebook-logo-transparent-black.png"
                                                                     style="border-radius:3px;display:block;"
                                                                     width="18"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table align="left" border="0" cellpadding="0" cellspacing="0" role="presentation"
                                           style="float:none;display:inline-table;">
                                        <tbody>
                                        <tr>
                                            <td style="padding:4px;">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                       style="border-radius:3px;width:18px;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="font-size:0;height:18px;vertical-align:middle;width:18px;">
                                                            <a href="#" target="_blank"
                                                               style="color: #2e58ff; text-decoration: none;">
                                                                <img alt="instagram-logo"
                                                                     height="18"
                                                                     src="https://codedmails.com/images/social/black/instagram-logo-transparent-black.png"
                                                                     style="border-radius:3px;display:block;"
                                                                     width="18"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

    <div style="margin:0 auto;max-width:600px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
            <tbody>
            <tr>
                <td style="direction:ltr;font-size:0;padding:20px 0;text-align:center;">
                    <div class="mj-column-per-100 mj-outlook-group-fix"
                         style="font-size:0;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                               style="vertical-align:top;" width="100%">
                            <tbody>
                            <tr>
                                <td style="font-size:0;word-break:break-word;">
                                    <div style="height:1px;"></div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</body>

</html>
