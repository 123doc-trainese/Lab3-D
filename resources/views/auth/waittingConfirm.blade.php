<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css"
          integrity="sha512-1sCRPdkRXhBV2PBLUdRb4tMg1w2YPf37qatUFeS7zlBy7jJI8Lf4VHwWfZZfpXtYSLy85pkm9GaYVYMfw5BC1A=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/brands.min.css"
          integrity="sha512-nS1/hdh2b0U8SeA8tlo7QblY6rY6C+MgkZIeRzJQQvMsFfMQFUKp+cgMN2Uuy+OtbQ4RoLMIlO2iF7bIEY3Oyg=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/fontawesome.min.css"
          integrity="sha512-R+xPS2VPCAFvLRy+I4PgbwkWjw1z5B5gNDYgJN5LfzV4gGNeRQyVrY7Uk59rX+c8tzz63j8DeZPLqmXvBxj8pA=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>

</head>

<body>

<header class="lg:px-10 lg:mb-4 font-roboto h-auto w-full">

    @if (session('errors'))
        <div class="w-full text-center text-lg h-8 text-white bg-red">
            <div> {{ session('errors') }} </div>
        </div>
    @endif
    @if (session('message'))
        <div class="w-full text-center text-lg h-8 text-white bg-green">
            <div> {{ session('message') }} </div>
        </div>
    @endif

    <div
        class="inline-flex bg-[#F0F0F0] lg:bg-white justify-between w-full lg:border-b border-dashed shadow-2xl lg:shadow-none fixed lg:static top-0 z-10">

        <nav class="py-1">
            <div id="dropdown-wrapper" class="relative px-3">
                <button class="lg:pt-3 inline-block" onclick="showMenu()">
                    <i class="fa-solid fa-bars fa-2x "></i>
                </button>
                <p class="text-2xl text-black tracking-tightest font-black hidden lg:inline-block ">
                    SECTIONS </p>
            </div>

            <div class="hidden absolute text-sm text-center bg-[#f7f5f5] drop-shadow-2xl z-10 mt-1 " id="dropdown-menu">
                <div class=" lg:hidden">
                    <div class="items-center py-2 border-b bg-[#f7f5f5] relative">
                        @if (Auth::check())
                            <button onclick="toggleMenu1()">
                                <div class="inline-flex gap-2">
                                    <i class="fa-solid fa-user mt-1"></i>
                                    <p> Hi, {{ Auth::user()->name }} </p>
                                </div>
                            </button>

                            <div id="menu1" class="hidden absolute px-2 -right-16 top-10 shadow-2xl bg-[#f7f5f5]">
                                <div class="py-2 hover:bg-[#F0F0F0] ">
                                    <a href="{{ route('web.profile') }}">Change Password</a>
                                </div>
                                <div class="py-2 hover:bg-[#F0F0F0]">
                                    <a href="{{ route('web.logout') }}">Login out</a>
                                </div>
                            </div>
                        @else
                            <button class="lg:inline-block lg:w-24 lg:h-10 ">
                                <a href="{{ route('web.login') }}">Log in</a></button>
                        @endif
                    </div>
                </div>
            </div>

        </nav>

        <div class="text-center text-gray py-1 lg:text-xl lg:pt-5">
            <p class="hidden lg:block">Friday, February 24, 2017</p>
            <p class="h-auto w-full text-center text-black text-2xl mg:py-3 lg:hidden font-bitter"><a
                    href="{{ route('web.home') }}"> <b>The News</b> </a>
            </p>
        </div>

        <div class="lg:text-right py-1 px-3 gap-3 lg:py-5 inline-flex">
            <div class="my-1 lg:my-0">
                <i class="fa-solid fa-magnifying-glass"></i>
                <input
                    class="hidden placeholder:px-4 focus:px-2 focus:outline border-none lg:w-36 lg:h-10 lg:inline-block"
                    type="search" size="20" placeholder="SEARCH">
            </div>
            @if (Auth::check())
                <div class="text-xl my-1 text-center hidden lg:inline-block relative">
                    <nav class="relative">
                        <button onclick="toggleMenu()">
                            <p class="lg:text-xl text-sm "> Hi, {{ Auth::user()->name }} </p>
                        </button>
                        <div id="menu"
                             class="hidden text-xs lg:text-lg bg-white drop-shadow-2xl absolute top-6 lg:top-12 -right-4 w-48">
                            <div class="py-2 hover:bg-[#F0F0F0] ">
                                <a href="{{ route('web.profile') }}">Change Password</a>
                            </div>
                            <div class="py-2 hover:bg-[#F0F0F0]">
                                <a href="{{ route('web.logout') }}">Login out</a>
                            </div>
                        </div>
                    </nav>
                </div>
            @else
                <button class="bg-sport hidden lg:inline-block lg:w-24 lg:h-10 "><a href="{{ route('web.login') }}">
                        Log in</a></button>
            @endif
        </div>

    </div>

    <div class=" w-full h-auto text-center lg:text-8xl lg:pt-3 lg:inline-block">
        <p class=" mt-5 text-center text-bitter hidden lg:block"><a href="{{ route('web.home') }}"> <b>The News</b>
            </a>
        </p>
    </div>

</header>

<div class="font-roboto my-24">
    <div class="mx-auto w-full">
        <div class="text-center text-2xl">
            <form action="{{ route('resend.email') }}" method="POST">
                @csrf
                <input type="hidden" name="email" value="{{ Auth::user()->email }}">

                <h2> Hi, {{ Auth::user()->name }} <br>
                    Your account has been created, please confirm by email!! <br>
                    Resend confirmation email to your email...
                </h2>
                @if (session('notice'))
                    <div class="w-full text-center text-lg h-8 text-white bg-gray">
                        <div> {{ session('notice') }} </div>
                    </div>
                @endif
                <button type="submit" class=" bg-[#000000] text-white w-1/4 lg:w-1/12 mt-10 py-1 h-10  ">
                    Resend email now.
                </button>
            </form>

        </div>
    </div>
</div>

<script>
    // profile menu
    function toggleMenu() {

        var menu = document.getElementById("menu");

        if (menu.classList.contains('hidden')) {
            menu.classList.remove('hidden');
        } else {
            menu.classList.add('hidden');
        }
    }

    // dropdown menu
    function showMenu() {

        var menu = document.getElementById("dropdown-menu");

        if (menu.classList.contains('hidden')) {
            menu.classList.remove('hidden');
        } else {
            menu.classList.add('hidden');
        }
    }

    // profile menu
    function toggleMenu1() {

        var menu = document.getElementById("menu1");

        if (menu.classList.contains('hidden')) {
            menu.classList.remove('hidden');
        } else {
            menu.classList.add('hidden');
        }
    }
</script>

</body>

</html>
