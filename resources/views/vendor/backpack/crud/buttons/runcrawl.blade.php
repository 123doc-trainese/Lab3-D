@if ($crud->hasAccess('run'))

    @switch($entry['status'])
        @case(\App\Models\CrawlSite::STATUS['NEW'])
            <a href="{{ route('runcrawl', $entry->getKey()) }}" class="btn btn-sm btn-link"><i
                    class="la la-arrow-down"></i> Crawl </a>
            @break
        @case(\App\Models\CrawlSite::STATUS['PENDING'])
            <a href="{{ route('stopcrawl', $entry->getKey()) }}" class="btn btn-sm btn-link"><i class="la la-ban"></i>
                Stop </a>
            @break
        @case(\App\Models\CrawlSite::STATUS['CRAWED'])
            <a href="{{ route('runcrawl', $entry->getKey()) }}" class="btn btn-sm btn-link"><i
                    class="la la-arrow-down"></i> Crawl </a>
            @break
        @case('')
            @break
        @default
            @break
    @endswitch
@endif

