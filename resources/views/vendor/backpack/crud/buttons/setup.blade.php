@if ($crud->hasAccess('setupfrequences'))
    <a href="{{ url($crud->route.'/setupfrequences') }}" class="btn btn-sm btn-link">
        <button type="button" class="btn btn-primary">
            <i class="la la-calendar-check-o"></i>
            Set time crawl
        </button>
    </a>
@endif
