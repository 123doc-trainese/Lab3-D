@extends(backpack_view('blank'))

@section('content')
    {{-- TODO: SHOW FORM HERE --}}
    @section('header')
        <section class="container-fluid">
            <h2>
                <span class="text-capitalize">Setup time.</span>
                @if ($crud->hasAccess('list'))
                    <small>
                        <a href="{{ url($crud->route) }}" class="d-print-none font-sm">
                            <i
                                class="la la-angle-double-{{ config('backpack.base.html_direction') == 'rtl' ? 'right' : 'left' }}"></i>
                            {{ trans('backpack::crud.back_to_all') }}
                            <span>{{ $crud->entity_name_plural }}</span>
                        </a>
                    </small>
                @endif
            </h2>
        </section>
    @endsection

    @section('content')
        <div class="row">
            <div class="col-md-8 bold-labels">
                @if ($errors->any())
                    <div class="alert alert-danger pb-0">
                        <ul class="list-unstyled">
                            @foreach ($errors->all() as $error)
                                <li><i class="la la-info-circle"></i> {{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="post" action="{{ route('setuptime.post')  }}">
                    @csrf
                    <div class="card">
                        <div class="card-body row">

                            <div class="form-group col-md-2">
                                <label>Time Loop</label>
                                <div class="form-check">
                                    <input class="form-check-input"
                                           type="checkbox" value="1"
                                           name="check">
                                    <label class="form-check-label" for="flexCheckDefault">
                                        Yes/No
                                    </label>
                                </div>
                                @error('reply_to')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-md-2">
                                <label>Minute</label>
                                <input type="number" name="minute" min="0" max="59"
                                       class="form-control @error('minute') is-invalid @enderror">
                                @error('from')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-md-2">
                                <label>Hour</label>
                                <input type="number" name="hour" min="0" max="23"
                                       class="form-control @error('hour') is-invalid @enderror">
                                @error('from')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-md-2">
                                <label>Day</label>
                                <input type="number" name="day" min="1" max="31"
                                       class="form-control @error('day') is-invalid @enderror">
                                @error('reply_to')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-md-2">
                                <label>Month</label>

                                @foreach(\App\Models\CrawlSite::MONTH as $key => $value)
                                    <div class="form-check">
                                        <input class="form-check-input @error('month') is-invalid @enderror "
                                               type="checkbox" value="{{ $value }}"
                                               name="month[]">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            {{ $key}}
                                        </label>
                                    </div>
                                @endforeach

                                @error('reply_to')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-md-2">
                                <label>Day of the Week</label>

                                @foreach(\App\Models\CrawlSite::DAY as $key => $value)
                                    <div class="form-check">
                                        <input class="form-check-input @error('day_of_week') is-invalid @enderror"
                                               type="checkbox" value="{{ $value }}"
                                               name="day_of_week[]">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            {{ $key}}
                                        </label>
                                    </div>
                                @endforeach

                                @error('reply_to')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div id="saveActions" class="form-group">
                        <button type="submit" class="btn btn-success">
                            <span class="la la-save" role="presentation" aria-hidden="true"></span> &nbsp;
                            <span data-value="send_email">Save setup</span>
                        </button>
                        <a href="{{ url($crud->route) }}" class="btn btn-default"><span class="la la-ban"></span>
                            &nbsp;Cancel</a>
                    </div>
                </form>
            </div>
        </div>

    @endsection
