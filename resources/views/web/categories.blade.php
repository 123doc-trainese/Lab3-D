@php use Carbon\Carbon; @endphp
@extends('web.layout.master')

@section('content')

    <div class="font-roboto static">

        <div class="lg:grid lg:grid-cols-5 lg:gap-3 mx-3 xl:mx-20 lg:my-0 my-10 relative lg:static">

            <div class="lg:col-span-4 mx-5 lg:pt-3 lg:mt-3">
                <div class="py-2 px-4 text-lg lg:text-2xl bg-gray">{{ $posts[0]->category->name }}</div>

                <div class="text-right text-sm my-3">
                    <div class="lg:inline-flex gap-2 lg:gap-5 hidden ">
                        <div class="border rounded-lg">
                            <label>
                                <input class="border-none text-sm rounded-lg lg:h-10 pl-2" placeholder="" type="date">
                            </label>
                        </div>
                        <div>
                            <label>
                                <select class="border lg:h-10 rounded-lg p-2 text-sm bg-white">
                                    <option value="1">Trends</option>
                                    <option value="2">News</option>
                                    <option value="3">Hot</option>
                                    <option value="4">View More</option>
                                </select>
                            </label>
                        </div>
                        <div>
                            <label>
                                <select class="border lg:h-10 rounded-lg p-2 text-sm bg-white">
                                    <option value="1" class="text-sm">Category</option>
                                    <option value="2" class="text-sm">Sport</option>
                                    <option value="3" class="text-sm">Entertainment</option>
                                    <option value="4" class="text-sm">Travel</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="px-2 lg:px-10 lg:border rounded-lg">
                    @foreach ($posts as $key => $post)
                        <a href="{{ route('web.detail', $post->slug)}}">
                            <div class="block lg:inline-flex justify-between my-4 relative w-full">

                                <div class="mx-0 lg:mx-3 mt-3 lg:w-1/3">
                                    <img
                                        src="{{ $post->image? :'https://s1cdn.vnecdn.net/vnexpress/restruct/i/v654/logo_default.jpg' }}"
                                        alt="" class="object-cover w-full">
                                </div>

                                <div class="lg:boder-b lg:border-none lg:w-2/3">
                                    <h2 class="text-lg xl:text-2xl my-2"><b> {{ $post->title }} </b></h2>
                                    <p class="text-sm xl:text-xl my-6 w-2/3 lg:w-full hidden lg:block ">{!! $post->description !!}</p>
                                    <p class="text-gray my-2 text-base xl:text-lg"> {{ Carbon::parse($post->created_at)->format('l d-M-Y | h:i A') }}</p>
                                </div>
                            </div>
                        </a>

                    @endforeach
                </div>

                <div class="w-full my-4 h-auto inline-block">
                    {!! $posts->links() !!}
                </div>

            </div>

            <div
                class="lg:col-span-1 mx-1 my-4 -top-8 border-none rounded-lg lg:boder-b lg:border-none absolute lg:static ">

                <div class="my-5 hidden lg:block ">
                    <div class="text-xl lg:text-2xl py-2 w-32 px-2 text-center ">Category</div>

                    <div class="border  rounded-md my-5 py-5 h-56 overflow-y-auto">
                        @foreach ($all_category as $key => $category)
                            <a class="hover:text-blue"
                               href="{{ route('web.category.slug', $category->slug) }}">
                                <div
                                    class="border-b border-dashed border-gray-sl my-2 mx-10">{{ $category->name }}</div>
                            </a>
                        @endforeach
                    </div>

                    <div class="xl:inline-flex xl:justify-between w-full border-b border-[#0C79FF] text-sm xl:text-lg">
                        <label>
                            <input class="h-10 placeholder:pl-3" type="text" placeholder="Enter keywords">
                        </label>
                        {{--                        <button class="bg-[#0C79FF] text-white px-0 lg:px-2 py-2 lg:my-0 my-2 h-10 w-1/3"><b>Search</b>--}}
{{--                        </button>--}}
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection
