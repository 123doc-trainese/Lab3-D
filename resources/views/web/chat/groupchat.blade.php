@php use App\Models\Conversation;use App\Models\User; @endphp
@if (auth()->user())
    <div
        class="fixed chat-action bg-[#1E90FF] w-12 h-12 rounded-full bottom-10 right-5 z-20 shadow-2xl border border-green">
        <button type="button" onclick="showChat()" class="w-full h-full text-center text-white hover:shadow-2xl">
            <i class="fa-brands fa-rocketchat mx-auto my-auto"></i>
            <div data-count="{{ Conversation::countAllNewMessage() }}"
                 style="{{ Conversation::countAllNewMessage() > 0 ? "display: block;" : "display: none;"}}"
                 class="text-white text-sm absolute -bottom-1 -right-1 bg-red rounded-full w-5"> {{ Conversation::countAllNewMessage() }}
            </div>
        </button>
    </div>

    <div id="my-chat" class="fixed hidden bottom-24 right-5 z-20">
        <button type="button" onclick="openCloseChatBox()"
                class="bg-[#00CED1] w-12 h-12 my-1.5 rounded-full text-center text-white hover:shadow-2xl table">
            <i class="fa-solid fa-circle-plus"></i>
        </button>

        <button type="button" onclick="getGroup()"
                class="bg-[#00CED1] w-12 h-12 rounded-full z-20 text-center text-white hover:shadow-2xl table">
            <i class="fa-solid fa-people-group"></i>
        </button>
    </div>

    <div id="chat-box" class="fixed bottom-3 right-20 inline-flex rounded-xl gap-2 z-20">
        <div class="relative">
            <div id="box-create-group"
                 class="hidden lg:w-80 w-[14rem] h-28 text-sm z-20 bg-[#EAEAEA] absolute bottom-0 right-0 rounded-xl">
                <div class="inline-flex justify-between w-full p-3">
                    <p>Tin nhắn mới</p>
                    <div class="">
                        <button type="button" onclick="openCloseChatBox()"
                                class="bg-white rounded-full px-2 items-center"><i class="fa-solid fa-xmark"></i>
                        </button>
                    </div>
                </div>
                <div class="inline-flex justify-between gap-3 w-full p-3">
                    <label for="target"></label>
                    <input type="text" id="target"
                           class="w-full px-2 py-2 border-none rounded-xl placeholder:border-none placeholder:text-xs"
                           placeholder="Group Name">
                    <button type="button" onclick="createGroupChat()" class="bg-blue text-white mx-1 px-3 rounded-xl">
                        Create
                    </button>
                </div>
            </div>
        </div>

        <div class="relative">
            <div class="hidden bg-white w-56 lg:w-80 h-auto my-1 rounded-lg z-20 shadow-2xl absolute bottom-0 right-0"
                 id="my-group">
                <ul class="space-y-2 max-h-[28rem] overflow-y-auto" id="listgroup"></ul>
            </div>
        </div>

        <div id="chat-door" class="inline-flex gap-2"></div>
    </div>

    <!-- Overlay element -->
    <div id="overlay" class="fixed hidden z-30 w-screen h-screen inset-0 bg-gray bg-opacity-60"></div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" crossorigin="anonymous"
            referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://js.pusher.com/7.2/pusher.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>

    <script type="text/javascript">

        function showChat() {
            document.getElementById('my-chat').classList.toggle("hidden");

        }

        function openCloseChatBox() {
            if (!document.getElementById('my-group').classList.contains("hidden")) {
                document.getElementById('my-group').classList.add("hidden")
            }

            document.getElementById('box-create-group').classList.toggle("hidden");
        }

        function getGroup() {
            $.ajax({
                type: "GET",
                url: '{{ route('group.chat.list') }}',
                data: {
                    _token: "{{ csrf_token() }}",
                },
                success: function (data) {
                    let show;
                    let html = '';

                    for (let i = 0; i < data.length; i++) {

                        if (data[i].count > 0) {
                            show = `<div class="text-white bg-red rounded-full w-6 h-6">` + data[i].count + `</div>`;
                        } else {
                            show = ``;
                        }

                        html += `<li class="inline-flex justify-between w-full p-3 hover:text-blue text-sm lg:text-base">
                                    <button type="submit" onclick="getChat('` + data[i].group.group_id + `',` + data[i].count + `)" class="w-full inline-flex gap-2 ">
                                        <i class="fa-regular fa-comment my-auto"></i>
                                        <p>Group: <span> ` + data[i].group.name + ` </span></p>`;
                        html += show;
                        html += `</button>
                                </li>`;
                    }
                    document.getElementById('listgroup').innerHTML = html;
                    document.getElementById('my-group').classList.toggle("hidden");

                    if (!document.getElementById('box-create-group').classList.contains("hidden")) {
                        document.getElementById('box-create-group').classList.add("hidden")
                    }
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        }

        function getChat(id, count) {
            if (ElementCount > 0) {
                ElementCount -= count;
                Element.attr('data-count', ElementCount);
            }

            if (ElementCount <= 0) {
                Element.hide();
            }
            Element.html(ElementCount);

            $('#my-group').addClass('hidden');

            if ($('#group-' + id).length) {
                document.getElementById('group-' + id).classList.remove('hidden');
            } else {
                $.ajax({
                    type: "GET",
                    url: '{{ route('group.chat.get') }}',
                    data: {
                        id: id,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function (data) {

                        let avata;
                        let style;
                        const chat_group = $(`#chat-door`).html();

                        let html = `<div id="group-` + id + `" class="bg-white w-72 lg:w-80 h-[28rem] lg:h-[30rem] rounded-xl z-20 shadow-2xl border border-gray">
                            <div class="max-w-2xl rounded-xl">
                                <div class="w-full">
                                    <div id="divcolor"
                                        class="relative inline-flex justify-between gap-2 w-full items-center rounded-t-xl p-3 bg-[#AAAAAA]">
                                        <div class="inline-flex gap-2 text-white">
                                            <button type="submit" onclick="addUser('` + id + `')"><i class="fas fa-user-plus"></i></button>
                                            <span class="block my-auto font-bold" id="group-name"> ` + data.group
                            .name + `</span>
                                        </div>
                                        <div class="mx-2">
                                            <button type="button" onclick="closeChatBox('group-` + id + `')"
                                                    class="bg-white rounded-full px-2 items-center"><i
                                                    class="fa-solid fa-xmark"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="relative w-full p-3">

                                        <ul class="space-y-2 overflow-y-auto h-[19rem] lg:h-[21rem]" id="ul-chat-` + id + `" >`;

                        for (let i = 0; i < data.con.length; i++) {
                            if (data.con[i].status === {{ Conversation::STATUS['ACCTION'] }}) {
                                html += `<li class="mx-auto w-1/2">
                                             <div class="text-xs text-[#c9c9c9]">` + data.con[i].message + `</div>
                                         </li>`;
                            } else {
                                if (data.con[i].user_id === {{ auth()->id() }}) {
                                    style = 'justify-end';
                                } else {
                                    style = '';
                                }

                                if (data.con[i].avata) {
                                    avata = data.con[i].avata;
                                } else {
                                    avata = '{{ User::AVATA }}';
                                }
                                html += `<li class="inline-flex w-full ` + style + `">
                                    <div class="my-auto">
                                        <img src="` + avata + `" alt="" class="rounded-full object-cover w-8">
                                    </div>
                                     <div class="relative max-w-xl mx-2 px-2 py-2">
                                           <p class="text-xs">` + data.con[i].name + `</p>
                                          <span class="bg-[#E4E6EB] px-2 py-1 rounded-md text-sm">` + data.con[i].message + `</span>
                                     </div>
                                 </li>`
                            }
                        }

                        html += `</ul>
                                    </div>
                                    <div class="inline-flex items-center justify-between gap-2 w-full p-2 bottom-0 border-t border-gray">
                                        <button>
                                            <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                                                 stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                      d="M14.828 14.828a4 4 0 01-5.656 0M9 10h.01M15 10h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"/>
                                            </svg>
                                        </button>

                                        <input type="text" placeholder="Message"
                                               class="block w-full py-2 pl-2 mx-1 rounded-full outline-none focus" name="message" id="sendmessage-` + id + `"
                                               required/>

                                        <button type="submit" onclick="sendMessage('` + data.group.id + `')">
                                            <svg class="w-5 h-5 origin-center transform rotate-90"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 viewBox="0 0 20 20" fill="currentColor">
                                                <path
                                                    d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z"/>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>`;

                        $('#chat-door').html(chat_group + html);

                        $('#ul-chat-' + id).scrollTop($('#ul-chat-' + id)[0].scrollHeight);

                        $('#group-' + id).click(function () {

                            $.ajax({
                                type: "GET",
                                url: '{{ route('group.chat.read') }}',
                                data: {
                                    id: id,
                                    _token: "{{ csrf_token() }}",
                                },
                                success: function (data) {
                                    if (ElementCount > 0) {
                                        $('#group-' + id).find('div#divcolor').removeClass('bg-[#00008B]');
                                        $('#group-' + id).find('div#divcolor').addClass('bg-[#AAAAAA]');

                                        ElementCount -= data;

                                        Element.attr('data-count', ElementCount);

                                        if (ElementCount <= 0) {
                                            Element.hide();
                                        }
                                        Element.html(ElementCount);
                                    }
                                },
                                error: function (xhr, status, error) {
                                    console.log(error);
                                }
                            });
                        });
                    },
                    error: function (xhr, status, error) {
                        console.log(error);
                    }
                });
            }
        }

        function sendMessage(group_id) {
            const message = $('#sendmessage-' + group_id).val();

            $.ajax({
                type: "POST",
                url: '{{ route('send.message.chat') }}',
                data: {
                    message: message,
                    group_id: group_id,
                    _token: "{{ csrf_token() }}",
                },
                success: function (data) {
                    const chat_group = $(`#ul-chat-` + group_id).html();

                    const html = `<li class="inline-flex w-full justify-end">
                                    <div class="my-auto">
                                        <img src="{{ auth()->user()->avata ?? User::AVATA }}" alt="" class="rounded-full object-cover w-8">
                                    </div>
                                     <div class="relative max-w-xl mx-2 px-2 py-2">
                                           <p class="text-xs">{{ auth()->user()->name }}</p>
                                          <span class="bg-[#E4E6EB] px-2 py-1 rounded-md text-sm">` + message + `</span>
                                     </div>
                                 </li>`;
                    $('#ul-chat-' + group_id).html(chat_group + html);
                    $('#sendmessage-' + group_id).val('');

                    $('#ul-chat-' + group_id).scrollTop($('#ul-chat-' + group_id)[0].scrollHeight);
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        }

        function createGroupChat() {
            const value = $('#target').val().toLowerCase();

            if (ElementCount > 0) {
                ElementCount -= count;
                Element.attr('data-count', ElementCount);
            }

            if (ElementCount <= 0) {
                Element.hide();
            }
            Element.html(ElementCount);

            if (value) {
                $.ajax({
                    type: "POST",
                    url: '/create-group',
                    data: {
                        name: value,
                        _token: "{{ csrf_token() }}",
                    },
                    success: function (data) {
                        const chat_group = $(`#chat-door`).html();

                        const html = `<div id="group-` + data.id + `" class="bg-white w-72 lg:w-80 h-[28rem] lg:h-[30rem] rounded-xl z-20 shadow-2xl border border-gray">
                            <div class="max-w-2xl rounded-xl">
                                <div class="w-full">
                                    <div id="divcolor"
                                        class="relative inline-flex justify-between gap-2 w-full items-center rounded-t-xl p-3 bg-[#AAAAAA]">
                                        <div class="inline-flex gap-2 text-white">
                                            <button type="submit" onclick="addUser('` + data.id + `')"><i class="fas fa-user-plus"></i></button>
                                            <span class="block my-auto font-bold" id="group-name"> ` + value + `</span>
                                        </div>
                                        <div class="mx-2">
                                            <button type="button" onclick="closeChatBox('group-` + data.id + `')"
                                                    class="bg-white rounded-full px-2 items-center"><i
                                                    class="fa-solid fa-xmark"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="relative w-full p-3">

                                        <ul class="space-y-2 overflow-y-auto h-[19rem] lg:h-[21rem]" id="ul-chat-` + data.id + `">
                                        </ul>
                                    </div>
                                    <div
                                        class="inline-flex items-center justify-between gap-2 w-full p-2 bottom-0 border-t border-gray">
                                        <button>
                                            <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                                                 stroke="currentColor">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                      d="M14.828 14.828a4 4 0 01-5.656 0M9 10h.01M15 10h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"/>
                                            </svg>
                                        </button>

                                        <input type="text" placeholder="Message"
                                               class="block w-full py-2 pl-2 mx-1 rounded-full outline-none focus" name="message" id="sendmessage-` + data.id + `"
                                               required/>

                                        <button type="submit" onclick="sendMessage('` + data.id + `')">
                                            <svg class="w-5 h-5 origin-center transform rotate-90"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 viewBox="0 0 20 20" fill="currentColor">
                                                <path
                                                    d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z"/>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>`;

                        openCloseChatBox();
                        $('#chat-door').html(html + chat_group);
                        document.getElementById('listgroup').classList.add("hidden");
                        $('#ul-chat-' + data.id).scrollTop($('#ul-chat-' + data.id)[0].scrollHeight);

                        $('#group-' + data.id).click(function () {

                            var groupid = data.id;

                            $.ajax({
                                type: "GET",
                                url: '{{ route('group.chat.read') }}',
                                data: {
                                    id: data.id,
                                    _token: "{{ csrf_token() }}",
                                },
                                success: function (data) {
                                    if (ElementCount > 0) {
                                        $('#group-' + groupid).find('div#divcolor').removeClass('bg-[#00008B]');
                                        $('#group-' + groupid).find('div#divcolor').addClass('bg-[#AAAAAA]');

                                        ElementCount -= data;

                                        Element.attr('data-count', ElementCount);

                                        if (ElementCount <= 0) {
                                            Element.hide();
                                        }
                                        Element.html(ElementCount);
                                    }
                                },
                                error: function (xhr, status, error) {
                                    console.log(error);
                                }
                            });
                        });
                    },
                    error: function (xhr, status, error) {
                        console.log(error);
                    }
                });
            }
        }

        function closeChatBox(id) {
            document.getElementById(id).classList.add("hidden");
        }

        // -----------------------------
        function addUser(id) {

            $.ajax({
                type: "POST",
                url: '/get-users',
                data: {
                    id: id,
                    _token: "{{ csrf_token() }}",
                },
                success: function (data) {
                    let avata;
                    let sizeData;
                    if (data) {

                        let html = `<div class="bg-white w-96 h-[28rem] mx-auto px-2 mt-36 rounded-xl shadow-2xl relative" id="listusers">
                            <div class="inline-flex justify-between w-full p-2 ">
                                <p>Add members</p>
                                <button type="button" onclick="closeAddUser()"
                                        class="bg-white rounded-full px-2 items-center"><i class="fa-solid fa-xmark"></i>
                                </button>
                            </div>
                            <div class="mx-auto w-11/12 absolute text-center py-1 text-red" id="message"></div>
                            <div class="inline-flex justify-between w-full mt-8 gap-2 my-3">
                                <input type="text" id="search-user" name="search-user" placeholder="Mời tham gia"
                                       class="w-full py-1 px-2">
                                <button type="submit" onclick="searchUser('` + id + `')" class="bg-blue text-white rounded-xl px-2 py-1">Search
                                </button>
                            </div>
                            <ul id="listuser" class="space-y-2 overflow-y-auto h-72">`;


                        sizeData = data.length;
                        for (let i = 0; i < sizeData; i++) {

                            if (data[i].avata) {
                                avata = data[i].avata;
                            } else {
                                avata = '{{ User::AVATA  }}';
                            }

                            html += `<li class="inline-flex gap-3 w-full">
                                        <div>
                                            <img src="` + avata + `" alt=""
                                                 class="rounded-full object-cover w-8">
                                        </div>
                                        <div class="mx-3 my-auto inline-flex justify-between w-full">
                                            <span class="block">` + data[i].name + `</span>`;
                            if ({{ auth()->user()->id }} === data[i].id) {
                                html += `<a href="/leave-group-chat/` + data[i].group_id +
                                    `" class="hover:text-blue"> Leave </a>`;
                            }
                            html += `</div>
                                    </li>`;
                        }

                        html += `</ul>
                        </div>`;

                        document.getElementById('overlay').innerHTML = html;
                        document.getElementById('overlay').classList.toggle("hidden");
                    }
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        }

        function closeAddUser() {
            document.getElementById('overlay').classList.toggle("hidden");
        }

        function searchUser(id) {
            const value = $('#search-user').val().toLowerCase();

            $.ajax({
                type: "POST",
                url: '/search-user',
                data: {
                    name: value,
                    _token: "{{ csrf_token() }}",
                },
                success: function (data) {
                    let avata;
                    let html;
                    let sizeData;
                    if (data) {
                        sizeData = data.length;
                        html = ''
                        for (let i = 0; i < sizeData; i++) {

                            if (data[i].avata) {
                                avata = data[i].avata;
                            } else {
                                avata = '{{ User::AVATA  }}';
                            }

                            html += `<li class="inline-flex gap-3 w-full">
                                        <div>
                                            <img src="` + avata + `" alt=""
                                                 class="rounded-full object-cover w-8">
                                        </div>
                                        <div class="mx-3 my-auto inline-flex justify-between w-full">
                                            <span class="block">` + data[i].name + `</span>
                                            <div id="saveid` + data[i].id +
                                `"><button type="submit" onclick="saveAddUser('` + data[i].id + `','` + id +
                                `','` + data[i].name + `')" class="hover:text-blue">
                                                    Mời</button></div>
                                        </div>
                                    </li>`;
                        }
                        document.getElementById('listuser').innerHTML = html;
                    }
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        }

        function saveAddUser(id_user, id_group, name) {
            $.ajax({
                type: "POST",
                url: '/save-user',
                data: {
                    id_user: id_user,
                    id_group: id_group,
                    _token: "{{ csrf_token() }}",
                },
                success: function (data) {
                    if (data) {
                        const chat_group = $(`#ul-chat-` + id_group).html();
                        let value;
                        value = `<li class="mx-auto w-1/2">
                                     <div class="text-xs text-[#c9c9c9]"> {{ auth()->user()->name }}. "đã thêm" . ` +
                            name + `. "vào nhóm chat" </div>
                                 </li>`;
                        $('#ul-chat-' + id_group).html(chat_group + value);
                    }

                    document.getElementById('message').innerText = data.message;
                    document.getElementById('saveid' + id_user).innerText = 'Hủy'
                    setTimeout(function () {
                        document.getElementById('message').innerText = "";
                    }, 2000);
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        }
    </script>
@endif
