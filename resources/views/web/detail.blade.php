@php use Carbon\Carbon; @endphp
@extends('web.layout.master')

@section('content')
    <div class="font-roboto">
        <div class="hidden lg:inline-flex gap-2 w-full px-24 ">
            <div class="border-l-8 border-gray">
                Home >
            </div>
            <div>
                {{ $post->category->name }} >
            </div>
            <div class=" text-blue">
                {{ $post->title }} >
            </div>
        </div>

        <div class="lg:grid lg:grid-cols-4 lg:gap-5 px-2 lg:mx-5 xl:mx-20 lg:my-3 ">

            <div class="lg:col-span-3">

                <div class="bg-gray-sl h-8 lg:px-3 my-5 py-1"><b>{{ $post->category->name }}</b></div>

                <div class="text-4xl my-5">
                    <b>{{ $post->title }}</b>
                </div>

                <div class="inline-flex justify-between w-full text-gray my-5">
                    <div class="inline-flex gap-3">
                        <div><i class="fa-solid fa-clock "></i>
                        </div>
                        <div>
                            <h3>{{ Carbon::parse($post->created_at)->format('l d-M-Y h:i A') }}</h3>
                        </div>
                    </div>
                    <div class=" inline-flex gap-3">
                        <div>
                            <i class="fa-solid fa-eye"></i> {{ $post->view_counts }}
                        </div>
                        <div>
                            <i class="fa-solid fa-comment-dots"></i> 1031
                        </div>
                    </div>
                </div>

                <div class="my-4 text-lg">
                    <p class="my-4 text-2xl "> {!! $post->description !!} </p>
                    <!-- <img src=" {{ $post->image }} " alt="" class="bg-cover bg-no-repeat bg-center w-full h-auto object-cover"> -->
                    <p class="py-4"> {!! $post->content !!} </p>
                    <!-- <p class="w-full text-right text-3xl my-10"> <b>John</b> </p> -->
                </div>

                <div class="inline-flex justify-between w-full">
                    <div class="text-gray inline-flex">
                        <div class="border px-4 py-2  "><i class="fa-solid fa-arrow-left-long "></i></div>
                        <div class="inline-flex border mx-5 px-5 py-2">
                            <div><i class="fa-solid fa-download mx-2"></i></div>
                            <div>Save</div>
                        </div>
                    </div>

                    <div class="inline-flex gap-4 py-2 mx-4">
                        <div>Share</div>
                        <div><i class="fa-brands fa-facebook"></i></div>
                        <div><i class="fa-brands fa-twitter"></i></div>
                        <div><i class="fa-solid fa-envelope"></i></div>
                    </div>
                </div>

                <div class="px-3 lg:px-10 my-5 border border-gray rounded-md ">

                    <div class="text-lg lg:text-2xl my-4 px-2">Comment</div>

                    @comments([
                    'model' => $post,
                    'approved' => true,
                    'perPage' => 2,
                    'maxIndentationLevel' => 1
                    ])

                </div>

            </div>

            <div class="lg:col-span-1 px-0 lg:px-2">

                <div class="my-5 hidden lg:block w-full">
                    <div class="text-xl lg:text-2xl w-32 text-center bg-gray-sl">Category</div>

                    <div class="border border-gray-sl rounded-md my-5 py-5 h-56 overflow-y-auto">
                        @foreach ($all_category as $key => $category)
                            <a class="hover:text-blue"
                               href="{{ route('web.category.slug', $category->slug) }}">
                                <div
                                    class="border-b border-dashed border-gray-sl my-2 mx-10">{{ $category->name }}</div>
                            </a>
                        @endforeach
                    </div>

                    <div class="inline-flex justify-between w-full border-b border-[#0C79FF]">
                        <label>
                            <input class="h-10 placeholder:pl-3 w-2/3 focus:border-none" type="text"
                                   placeholder="Enter keywords">
                        </label>
                        <button class="bg-[#0C79FF] text-white px-0  lg:px-2 py-2 h-10 w-1/3 text-lg"><b>Search</b>
                        </button>
                    </div>
                </div>

                <div class="w-full my-8">
                    <div class="bg-gray w-1/4 text-center mx-3 lg:mx-0 py-1 my-6">NEWS</div>
                    @foreach ($new_posts as $key => $post)
                        <a href="{{ route('web.detail', $post->slug) }}">
                            <div class="relative my-3">
                                <img
                                    src="{{ $post->image? :'https://s1cdn.vnecdn.net/vnexpress/restruct/i/v654/logo_default.jpg' }}"
                                    class="object-cover rounded-xl w-full h-auto" alt="">
                                <p class="absolute bottom-10 text-white text-base xl:text-2xl w-4/5 mx-2 xl:mx-6"><b>
                                        {{ $post->title }} </b></p>
                                <p class="absolute bottom-3 text-white mx-6">2:30 | By Telegraph</p>
                            </div>
                        </a>
                    @endforeach

                </div>

                <div class="w-full my-3">
                    <div class=" py-1 my-3 xl:w-1/3  bg-gray text-center">VIEW MORE</div>
                    @foreach ($relate as $key => $post)
                        <a href="{{ route('web.detail', $post->slug) }}">
                            <div class="my-3">
                                <p class="text-lg lg:text-2xl"><b>{{ $post->title }}</b></p>
                                <p>{!! $post->description !!}</p>
                            </div>
                            <div class="border-b border-gray-sl w-auto my-2"></div>
                        </a>
                    @endforeach

                </div>

            </div>

        </div>

        <div class="lg:grid lg:grid-cols-4 gap-3 px-3 xl:px-20 my-3 py-3 overflow-auto inline-flex w-full">

            @foreach ($most_views as $key => $post)
                <div class="p-5 border rounded-lg border-gray flex-shrink-0 w-full">
                    <a href="{{ route('web.detail', $post->slug) }}">
                        <img
                            src="{{ $post->image ? :'https://s1cdn.vnecdn.net/vnexpress/restruct/i/v654/logo_default.jpg' }}"
                            alt="" class="rounded-lg object-cover h-72 w-full">
                        <p class="line-clamp-2 my-3 text-base xl:text-2xl h-16 "><b>{{ $post->title }}</b></p>
                        <div class="inline-flex justify-between w-full text-gray my-5 text-xs xl:text-base">
                            <div class="inline-flex gap-3">
                                <div><i class="fa-solid fa-clock "></i>
                                </div>
                                <div>
                                    <h3>{{ Carbon::parse($post->created_at)->format('d M Y') }}</h3>
                                </div>
                            </div>
                            <div class=" inline-flex gap-3">
                                <div>
                                    <i class="fa-solid fa-eye"></i> {{ $post->view_counts }}
                                </div>
                                <div>
                                    <i class="fa-solid fa-comment-dots"></i> 1210
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach

        </div>

    </div>
@endsection
