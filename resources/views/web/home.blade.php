@extends('web.layout.master')

@section('title', 'Trang chu')

@section('content')

    <div class="font-roboto contents">
        <div class="lg:grid lg:grid-cols-4 lg:gap-3 xl:mx-10 xl:my-3 mx-3 py-10">
            @forelse($highlight_post as $key => $post)
                <div class="lg:my-5 col-span-2">
                    <div class="bg-cover bg-no-repeat bg-center h-108 w-full relative"
                         style="background-image: url('{{ $post->image }}')">
                        <a href="{{ route('web.detail', $post->slug) }}"
                           class="text-black absolute bg-white hover:text-blue font-serif w-5/6 p-2 lg:w-3/4 text-lg lg:text-xl xl:text-4xl -bottom-1 lg:-bottom-8 ">
                            <b> {{ $post->title }}</b>
                        </a>
                    </div>
                    <div class="mt-12">
                        <p class="text-base lg:text-xl text-slate  left-0 hidden lg:block">
                            {!! $post->description !!}
                        </p>
                    </div>
                </div>
            @empty
                <div class=" lg:my-5 col-span-2 bg-cover bg-no-repeat bg-center h-108 w-full relative">
                </div>
            @endforelse
            <div class="lg:px-3 lg:col-span-1 lg:py-2 py-10">
                <div class="inline-flex justify-between w-full">
                    <p class="text-sm lg:text-2xl px-3 my-3 py-2 h-9 lg:h-11 bg-yellow"> TOP STORIES </p>
                    <p class="text-sm lg:text-2xl pt-4 text-blue lg:hidden inline-block"> View all </p>
                </div>
                @isset($top_stories)
                    @foreach ($top_stories as $key => $post)
                        <div
                            class="px-3 my-3 hover:shadow-md hover:text-blue lg:text-xl border-dashed border-b line-clamp-2"
                            id="top-stories">
                            <a href="{{ route('web.detail', $post->slug) }}"> {{ $post->title }}</a>
                        </div>
                    @endforeach
                @endisset
            </div>


            <div class="lg:mx-3">

                <div class="py-2 xl:text-xl">

                    <div class="bg-[#f0f0f0] hover:shadow-md relative">

                        <h2
                            class="bg-yellow text-center text-sm lg:text-2xl w-32 lg:w-48 h-9 lg:h-11 px-3 my-2 py-2 -top-5 absolute lg:static">
                            QUICK BITES </h2>
                        @isset($quick_bites)

                            @foreach ($quick_bites as $key => $post)
                                <div class="px-5 py-10 lg:py-3 text-base xl:text-xl lg:text-3xl hover:text-blue ">
                                    <a href="{{ route('web.detail', $post->slug) }}">
                                        <p class="font-merriweather"><b> {{ $post->title }}</b></p>
                                    </a>
                                </div>
                                <div class="h-auto">
                                    <h3 class="px-5 py-1 line-clamp-6">{!! $post->description !!}</h3>
                                </div>

                                <div class="px-5 pt-3 text-right py-4">
                                    <a href="">
                                        <p class="text-blue hover:text-green"><b class="lg:inline-block hidden">
                                                Next </b><b> > </b></p>
                                    </a>
                                </div>
                            @endforeach
                        @endisset

                    </div>

                    <div class="pt-2 xl:block hidden">
                        <a class="text-blue text-2xl font-merriweather">
                            <p class="px-2"><b> Get the Quick Bites delivered to your inbox daily </b></p>
                        </a>
                        <div class="w-full border-b border-blue my-6 inline-flex justify-between">
                            <label>
                                <input class="w-full" type="text" placeholder="Enter your email">
                            </label>
                            <a href="#" type="submit" class="float-right">
                                <p class="bg-blue text-white py-2 w-24 my-auto text-center text-sm lg:text-lg">Sign
                                    Up</p>
                            </a>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="lg:grid lg:grid-cols-4 lg:gap-2 mx-2 lg:mx-10 lg:ml-10 py-4 ">

            <div class="lg:col-span-4 2xl:col-span-3 lg:border-t lg:border-b lg:my-2 lg:w-full">

                <div
                    class=" text-center justify-between inline-flex w-full mt-2 border-b bg-[#f0f0f0] lg:border-b-0 border-sport">

                    <div class="bg-sport px-3 h-auto text-lg xl:text-2xl "> FEATURE</div>

                    <div class="bg-[#f0f0f0] text-base xl:text-xl text-center xl:mx-2 hidden lg:block">Mahashivratri
                        symbolizes a
                        union of divinity with a purpore of overcoming darkness and injustice.
                    </div>

                    <div class=" text-blue text-base xl:text-xl mx-2 hidden lg:block"><a href="">View all</a></div>

                </div>

                <div class="hidden xl:block text-center font-merriweather inline-flex mt-4 my-4 lg:w-full"
                     id="owl-theme">
                    @isset($feature_posts)
                        @foreach ($feature_posts as $key => $post)
                            <div class="relative flex-shrink-0 w-full mx-2 lg:w-1/3">
                                <a href="{{ route('web.detail', $post->slug) }}">
                                    <p
                                        class="bg-white text-lg lg:text-xl -bottom-3 mx-2 lg:mx-6 py-2 w-5/6 absolute hover:text-blue">
                                        {{ $post->title }}</p>
                                    <img
                                        src="{{ $post->image ?: 'https://s1cdn.vnecdn.net/vnexpress/restruct/i/v654/logo_default.jpg' }}"
                                        alt="" class="h-52 w-full rounded-lg">
                                </a>
                            </div>
                        @endforeach
                    @endisset

                </div>
            </div>

            <div class="col-span-1 relative hidden 2xl:block mx-3 p-3">
                <div class="brightness-50 hover:brightness-100">
                    <div class="bg-cover bg-no-repeat rounded-lg w-96 h-64 mx-auto"
                         style="background-image: url(../image/video.png)">
                    </div>
                </div>
                <div class="inline-flex gap-2 absolute left-10 top-6">
                    <i class="fa-solid fa-circle fa-1x text-red my-auto"></i>
                    <p class="text-red text-xl my-auto">Live</p>

                </div>

                <div class="absolute left-10 top-20 text-4xl text-white px-3 py-2 border-2 rounded-full border-white ">
                    <i class="fa-solid fa-play"></i>
                </div>

                <div class="absolute text-white bottom-10 left-10 text-2xl w-2/3 hover:text-green">
                    <b> Watch Modi live from G8 Summit</b>
                </div>
            </div>

        </div>

        <div class="lg:grid lg:grid-cols-5 lg:gap-2 xl:ml-10 ml-3 my-2 h-auto ">

            <div class="lg:col-span-1 mx-1 lg:mx-5 border-none lg:boder-b lg:border-none">

                <div class="bg-[#3bb189] w-32 h-8 py-1 my-4 text-center">
                    <p> WORLD NEWS </p>
                </div>
                @empty(!$worldnew_posts)

                    @foreach ($worldnew_posts as $post)
                        <div class="lg:my-3 lg:text-2xl font-merriweather hover:text-blue"><a
                                href="{{ route('web.detail', $post->slug) }}"> <b>{{ $post->title }}</b></a>
                        </div>

                        <h2 class="my-6 text-base xl:text-xl">{!! $post->description !!}</h2>

                        <div class="border-t lg:border-none border-dashed bg-[#f0f0f0] w-1/4 my-2"></div>
                    @endforeach
                @endempty

            </div>

            <div class="lg:col-span-2 lg:border-l py-4">
                @foreach ($most_cmts as $key => $post)
                    @if ($key === 0)
                        <div class="px-2 my-4 lg:ml-3 mb-3">
                            <a href="{{ route('web.detail', $post->slug) }}">
                                <h2 class="lg:text-2xl font-merriweather hover:text-blue">
                                    <b> {{ $post->title }} </b>
                                </h2>
                                <div class="py-3 gap-3 border-b border-dashed justify-between inline-flex w-full">
                                    <p class="text-base xl:text-xl ">{!! $post->description !!}</p>
                                    <img
                                        src="{{ $post->image ?: 'https://s1cdn.vnecdn.net/vnexpress/restruct/i/v654/logo_default.jpg' }}"
                                        alt="" class="w-36 h-24 text-right object-cover">
                                </div>
                            </a>
                        </div>
                    @endif
                    @if ($key === 1)
                        <div class="lg:px-3 py-3 ml-3 lg:mb-5 hidden lg:block">

                            <a href="{{ route('web.detail', $post->slug) }}">
                                <h2 class="lg:text-2xl font-merriweather hover:text-blue">
                                    <b>{{ $post->title }}</b>
                                </h2>
                                <p class="border-b border-dashed py-3 text-base xl:text-xl w-full">{!! $post->description !!}
                                </p>
                            </a>
                        </div>
                    @endif
                    @if ($key === 2)
                        <div class="px-3 lg:ml-3 lg:mb-5">
                            <a href="{{ route('web.detail', $post->slug) }}">
                                <h2 class="lg:text-2xl font-merriweather hover:text-blue">
                                    <b>{{ $post->title }}
                                    </b>
                                </h2>
                                <div class="py-3 gap-3 border-b border-dashed justify-between inline-flex w-full">
                                    <p class="text-base xl:text-xl">{!! $most_cmts[2]->description !!}</p>
                                    <img
                                        src="{{ $post->image ?: 'https://s1cdn.vnecdn.net/vnexpress/restruct/i/v654/logo_default.jpg' }}"
                                        alt="" class="w-36 h-24 text-right object-cover">
                                </div>
                            </a>
                        </div>
                    @endif
                    @if ($key === 3)
                        <div class="px-3 lg:ml-3 mb-5 hidden lg:block">
                            <a href="{{ route('web.detail', $post->slug) }}">
                                <h2 class="text-2xl hover:text-blue"><b> {{ $post->title }}</b></h2>
                                <p class="text-base xl:text-xl w-full">{!! $post->description !!}</p>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

            <div class="px-0 lg:px-4 lg:col-span-1  lg:border-r lg:border-l ">

                @empty(!$entertaiment_posts)

                    <div class="bg-yellow w-36 h-8 py-1 my-4 text-center">
                        <p class="lg:block hidden">ENTERTAIMENT</p>
                        <p class="lg:hidden">TOP STORIES</p>
                    </div>

                    <div class="px-4">
                        @foreach ($entertaiment_posts as $key => $post)
                            <a href="{{ route('web.detail', $post->slug) }}">
                                <div class="py-2 border-b border-dashed text-base xl:text-lg hover:text-blue">
                                    {{ $post->title }}</div>
                            </a>
                        @endforeach
                    </div>
                @endempty

            </div>

            <div class="px-0 lg:px-4 lg:col-span-1 mr-3 xl:mr-10">
                <div class="bg-sport w-32 h-8 py-1 my-4 text-center">
                    <p class="lg:block hidden">SPORTS NEWS</p>
                    <p class="lg:hidden">WORLD NEWS</p>
                </div>

                @foreach ($sport_posts as $key => $post)
                    @if ($key === 1)
                        <a href="{{ route('web.detail', $sport_posts[1]->slug) }}">
                            <div class="pt-3 text-base lg:text-xl xl:text-2xl font-merriweather hover:text-blue">
                                <b>{{ $sport_posts[1]->title }}</b>
                            </div>
                        </a>
                        <div class="pt-2 my-8 text-lg xl:text-xl">{!! $sport_posts[1]->description !!}</div>
                    @endif

                    @if ($key === 0)
                        <div class="w-full lg:border-b border-dashed "></div>
                        <div class="inline-flex lg:block py-2">
                            <img class="pt-2 w-36 gap-5 h-24 lg:w-full lg:h-48 object-cover"
                                 src="{{ $sport_posts[0]->image ?: 'https://s1cdn.vnecdn.net/vnexpress/restruct/i/v654/logo_default.jpg' }}"
                                 alt="">
                            <a href="{{ route('web.detail', $sport_posts[0]->slug) }}">
                                <div
                                    class="py-2 px-6 font-merriweatherw-full lg:px-0 text-lg lg:text-2xl  hover:text-blue">
                                    <b>{{ $sport_posts[0]->title }}</b>
                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>

        <div class="lg:grid lg:grid-cols-5 lg:gap-2 mx-3 xl:mx-10 lg:my-3 lg:h-full grid-flow-row">

            <div class="lg:col-span-4 ">

                <div class="bg-sport w-full h-10 justify-between hidden lg:inline-flex">
                    <div>
                        <p class="px-5 py-2 font-merriweather text-2xl "><b>Videos</b></p>
                    </div>

                    <div>
                        <a href="">
                            <p class="px-5 py-2 text-blue"><b> View all Videos </b></p>
                        </a>
                    </div>

                </div>

                <div class="xl:grid xl:grid-cols-4 xl:gap-2 lg:mb-3 lg:h-168 lg:border-b lg:border-dashed lg:border-l">

                    <div class="2xl:col-span-3 lg:col-span-4 mx-auto my-8  ">

                        <ul class="tabs w-11/12 mx-6 text-center justify-around hidden lg:inline-flex">
                            <li data-tab-target="#trending" class="tab hover:text-green active">Trending</li>
                            <li data-tab-target="#news" class="tab hover:text-green">News</li>
                            <li data-tab-target="#entertainment" class="tab hover:text-green">Entertainment</li>
                            <li data-tab-target="#celebs" class="tab hover:text-green">Celebs</li>
                            <li data-tab-target="#movie" class="tab hover:text-green">Movie</li>
                            <li data-tab-target="#lifestyle" class="tab hover:text-green">Lifestyle</li>
                            <li data-tab-target="#sports" class="tab hover:text-green">Sports</li>
                            <li data-tab-target="#tech" class="tab hover:text-green">Tech</li>
                            <li data-tab-target="#business" class="tab hover:text-green">Business</li>
                            <li data-tab-target="#auto" class="tab hover:text-green ">Auto</li>
                        </ul>

                        <div class="tab-content ">

                            <div id="trending" data-tab-content class="active">
                                <div class="py-4 flex-shrink-0 relative">
                                    <div
                                        class="absolute left-2 lg:left-8 bottom-8 lg:bottom-28 lg:text-4xl text-white lg:px-6 lg:py-4 px-4 py-2 border-2 rounded-full border-white">
                                        <i class="fa-solid fa-play"></i>
                                    </div>
                                    <p
                                        class="absolute bottom-10 lg:bottom-24 ml-20 lg:ml-36 lg:w-128 text-sm lg:text-2xl xl:text-4xl text-white font-merriweather">
                                        Isro launches 104 satellites in a
                                        single mission to create world record</p>
                                    <p
                                        class="absolute bottom-6 lg:bottom-10 ml-20 lg:ml-36 lg:w-108 lg:text-2xl text-sm text-white ">
                                        2:30 | By Telegraph</p>
                                    <img src="../image/tenlua.png" alt=""
                                         class="2xl:w-256 lg:h-144 lg:w-full object-cover">
                                </div>
                            </div>

                            <div id="news" data-tab-content class="">
                                <div class="py-4 flex-shrink-0 relative">
                                    <div
                                        class="absolute left-2 lg:left-8 bottom-8 lg:bottom-28 lg:text-4xl text-white lg:px-6 lg:py-4 px-4 py-2 border-2 rounded-full border-white">
                                        <i class="fa-solid fa-play"></i>
                                    </div>
                                    <p
                                        class="absolute bottom-10 lg:bottom-24 ml-20 lg:ml-36 lg:w-128 text-sm lg:text-2xl xl:text-4xl text-white font-merriweather">
                                        Isro launches 104 satellites in a
                                        single mission to create world record</p>
                                    <p
                                        class="absolute bottom-6 lg:bottom-10 ml-20 lg:ml-36 lg:w-108 lg:text-2xl text-sm text-white ">
                                        2:30 | By Telegraph</p>
                                    <img src="../image/tongthong.png" alt=""
                                         class="2xl:w-256 lg:h-144 lg:w-full object-cover">
                                </div>
                            </div>

                            <div id="entertainment" data-tab-content class="">
                                <div class="py-4 flex-shrink-0 relative">
                                    <div
                                        class="absolute left-2 lg:left-8 bottom-8 lg:bottom-28 lg:text-4xl text-white lg:px-6 lg:py-4 px-4 py-2 border-2 rounded-full border-white">
                                        <i class="fa-solid fa-play"></i>
                                    </div>
                                    <p
                                        class="absolute bottom-10 lg:bottom-24 ml-20 lg:ml-36 lg:w-128 text-sm lg:text-2xl xl:text-4xl text-white font-merriweather">
                                        Isro launches 104 satellites in a
                                        single mission to create world record</p>
                                    <p
                                        class="absolute bottom-6 lg:bottom-10 ml-20 lg:ml-36 lg:w-108 lg:text-2xl text-sm text-white ">
                                        2:30 | By Telegraph</p>
                                    <img src="../image/tenlua.png" alt=""
                                         class="2xl:w-256 lg:h-144 lg:w-full object-cover">
                                </div>
                            </div>

                            <div id="celebs" data-tab-content class="">
                                <div class="py-4 flex-shrink-0 relative">
                                    <div
                                        class="absolute left-2 lg:left-8 bottom-8 lg:bottom-28 lg:text-4xl text-white lg:px-6 lg:py-4 px-4 py-2 border-2 rounded-full border-white">
                                        <i class="fa-solid fa-play"></i>
                                    </div>
                                    <p
                                        class="absolute bottom-10 lg:bottom-24 ml-20 lg:ml-36 lg:w-128 text-sm lg:text-2xl xl:text-4xl text-white font-merriweather">
                                        Isro launches 104 satellites in a
                                        single mission to create world record</p>
                                    <p
                                        class="absolute bottom-6 lg:bottom-10 ml-20 lg:ml-36 lg:w-108 lg:text-2xl text-sm text-white ">
                                        2:30 | By Telegraph</p>
                                    <img src="../image/tongthong.png" alt=""
                                         class="2xl:w-256 lg:h-144 lg:w-full object-cover">
                                </div>
                            </div>

                            <div id="movie" data-tab-content class="">
                                <div class="py-4 flex-shrink-0 relative">
                                    <div
                                        class="absolute left-2 lg:left-8 bottom-8 lg:bottom-28 lg:text-4xl text-white lg:px-6 lg:py-4 px-4 py-2 border-2 rounded-full border-white">
                                        <i class="fa-solid fa-play"></i>
                                    </div>
                                    <p
                                        class="absolute bottom-10 lg:bottom-24 ml-20 lg:ml-36 lg:w-128 text-sm lg:text-2xl xl:text-4xl text-white font-merriweather">
                                        Isro launches 104 satellites in a
                                        single mission to create world record</p>
                                    <p
                                        class="absolute bottom-6 lg:bottom-10 ml-20 lg:ml-36 lg:w-108 lg:text-2xl text-sm text-white ">
                                        2:30 | By Telegraph</p>
                                    <img src="../image/tenlua.png" alt=""
                                         class="2xl:w-256 lg:h-144 lg:w-full object-cover">
                                </div>
                            </div>

                            <div id="lifestyle" data-tab-content class="">
                                <div class="py-4 flex-shrink-0 relative">
                                    <div
                                        class="absolute left-2 lg:left-8 bottom-8 lg:bottom-28 lg:text-4xl text-white lg:px-6 lg:py-4 px-4 py-2 border-2 rounded-full border-white">
                                        <i class="fa-solid fa-play"></i>
                                    </div>
                                    <p
                                        class="absolute bottom-10 lg:bottom-24 ml-20 lg:ml-36 lg:w-128 text-sm lg:text-2xl xl:text-4xl text-white font-merriweather">
                                        Isro launches 104 satellites in a
                                        single mission to create world record</p>
                                    <p
                                        class="absolute bottom-6 lg:bottom-10 ml-20 lg:ml-36 lg:w-108 lg:text-2xl text-sm text-white ">
                                        2:30 | By Telegraph</p>
                                    <img src="../image/tongthong.png" alt=""
                                         class="2xl:w-256 lg:h-144 lg:w-full object-cover">
                                </div>
                            </div>

                            <div id="sports" data-tab-content class="">
                                <div class="py-4 flex-shrink-0 relative">
                                    <div
                                        class="absolute left-2 lg:left-8 bottom-8 lg:bottom-28 lg:text-4xl text-white lg:px-6 lg:py-4 px-4 py-2 border-2 rounded-full border-white">
                                        <i class="fa-solid fa-play"></i>
                                    </div>
                                    <p
                                        class="absolute bottom-10 lg:bottom-24 ml-20 lg:ml-36 lg:w-128 text-sm lg:text-2xl xl:text-4xl text-white font-merriweather">
                                        Isro launches 104 satellites in a
                                        single mission to create world record</p>
                                    <p
                                        class="absolute bottom-6 lg:bottom-10 ml-20 lg:ml-36 lg:w-108 lg:text-2xl text-sm text-white ">
                                        2:30 | By Telegraph</p>
                                    <img src="../image/tenlua.png" alt=""
                                         class="2xl:w-256 lg:h-144 lg:w-full object-cover">
                                </div>
                            </div>

                            <div id="tech" data-tab-content class="">
                                <div class="py-4 flex-shrink-0 relative">
                                    <div
                                        class="absolute left-2 lg:left-8 bottom-8 lg:bottom-28 lg:text-4xl text-white lg:px-6 lg:py-4 px-4 py-2 border-2 rounded-full border-white">
                                        <i class="fa-solid fa-play"></i>
                                    </div>
                                    <p
                                        class="absolute bottom-10 lg:bottom-24 ml-20 lg:ml-36 lg:w-128 text-sm lg:text-2xl xl:text-4xl text-white font-merriweather">
                                        Isro launches 104 satellites in a
                                        single mission to create world record</p>
                                    <p
                                        class="absolute bottom-6 lg:bottom-10 ml-20 lg:ml-36 lg:w-108 lg:text-2xl text-sm text-white ">
                                        2:30 | By Telegraph</p>
                                    <img src="../image/tongthong.png" alt=""
                                         class="2xl:w-256 lg:h-144 lg:w-full object-cover">
                                </div>
                            </div>

                            <div id="business" data-tab-content class="">
                                <div class="py-4 flex-shrink-0 relative">
                                    <div
                                        class="absolute left-2 lg:left-8 bottom-8 lg:bottom-28 lg:text-4xl text-white lg:px-6 lg:py-4 px-4 py-2 border-2 rounded-full border-white">
                                        <i class="fa-solid fa-play"></i>
                                    </div>
                                    <p
                                        class="absolute bottom-10 lg:bottom-24 ml-20 lg:ml-36 lg:w-128 text-sm lg:text-2xl xl:text-4xl text-white font-merriweather">
                                        Isro launches 104 satellites in a
                                        single mission to create world record</p>
                                    <p
                                        class="absolute bottom-6 lg:bottom-10 ml-20 lg:ml-36 lg:w-108 lg:text-2xl text-sm text-white ">
                                        2:30 | By Telegraph</p>
                                    <img src="../image/tenlua.png" alt=""
                                         class="2xl:w-256 lg:h-144 lg:w-full object-cover">
                                </div>
                            </div>

                            <div id="auto" data-tab-content class="">
                                <div class="py-4 flex-shrink-0 relative">
                                    <div
                                        class="absolute left-2 lg:left-8 bottom-8 lg:bottom-28 lg:text-4xl text-white lg:px-6 lg:py-4 px-4 py-2 border-2 rounded-full border-white">
                                        <i class="fa-solid fa-play"></i>
                                    </div>
                                    <p
                                        class="absolute bottom-10 lg:bottom-24 ml-20 lg:ml-36 lg:w-128 text-sm lg:text-2xl xl:text-4xl text-white font-merriweather">
                                        Isro launches 104 satellites in a
                                        single mission to create world record</p>
                                    <p
                                        class="absolute bottom-6 lg:bottom-10 ml-20 lg:ml-36 lg:w-108 lg:text-2xl text-sm text-white ">
                                        2:30 | By Telegraph</p>
                                    <img src="../image/tongthong.png" alt=""
                                         class="2xl:w-256 lg:h-144 lg:w-full object-cover">
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="lg:col-span-1 w-full h-160 border-l overflow-y-auto mt-4 py-2 hidden 2xl:block">

                        <div class="relative w-4/5 mx-auto my-4">
                            <div
                                class="absolute text-4xl text-white ml-3 mt-3 px-3 py-1 border-2 rounded-full border-white">
                                <i class="fa-solid fa-play"></i>
                            </div>
                            <p class="absolute text-white bottom-1 w-full mx-1 text-lg font-merriweather">Isro launches
                                104
                                satellites in a single
                                mission </p>
                            <img src="../image/menu-img1.png " alt="" class="object-cover">
                        </div>

                        <div class="relative w-4/5 mx-auto my-4">
                            <div
                                class="absolute text-4xl text-white ml-3 mt-3 px-3 py-1 border-2 rounded-full border-white">
                                <i class="fa-solid fa-play"></i>
                            </div>
                            <p class="absolute text-white bottom-1 w-full mx-1 text-lg font-merriweather">Isro launches
                                104
                                satellites in a single
                                mission </p>
                            <img src="../image/menu-img2.png" alt="" class="object-cover">
                        </div>

                        <div class="relative w-4/5 mx-auto my-4">
                            <div
                                class="absolute text-4xl text-white ml-3 mt-3 px-3 py-1 border-2 rounded-full border-white">
                                <i class="fa-solid fa-play"></i>
                            </div>
                            <p class="absolute text-white bottom-1 w-full mx-1 text-lg font-merriweather">Isro launches
                                104
                                satellites in a single
                                mission </p>
                            <img src="../image/menu-img3.png" alt="" class="object-cover">
                        </div>

                        <div class="relative w-4/5 mx-auto my-4">
                            <div
                                class="absolute text-4xl text-white ml-3 mt-3 px-3 py-1 border-2 rounded-full border-white">
                                <i class="fa-solid fa-play"></i>
                            </div>
                            <p class="absolute text-white bottom-1 w-full mx-1 text-lg font-merriweather">Isro launches
                                104
                                satellites in a single
                                mission </p>
                            <img src="../image/menu-img1.png" alt="" class="object-cover">
                        </div>

                        <div class="relative w-4/5 mx-auto my-4">
                            <div
                                class="absolute text-4xl text-white ml-3 mt-3 px-3 py-1 border-2 rounded-full border-white">
                                <i class="fa-solid fa-play"></i>
                            </div>
                            <p class="absolute text-white bottom-1 w-full mx-1 text-lg font-merriweather">Isro launches
                                104
                                satellites in a single
                                mission </p>
                            <img src="../image/menu-img2.png" alt="" class="object-cover">
                        </div>

                        <div class="relative w-4/5 mx-auto my-4">
                            <div
                                class="absolute text-4xl text-white ml-3 mt-3 px-3 py-1 border-2 rounded-full border-white">
                                <i class="fa-solid fa-play"></i>
                            </div>
                            <p class="absolute text-white bottom-1 w-full mx-1 text-lg font-merriweather">Isro launches
                                104
                                satellites in a single
                                mission </p>
                            <img src="../image/menu-img3.png" alt="" class="object-cover">
                        </div>

                    </div>
                </div>

                <div class="lg:grid lg:grid-cols-5 lg:gap-4 mx-2 my-6 l g:pt-10">
                    @foreach ($new_posts as $post)
                        <a href="{{ route('web.detail', $post->slug) }}">
                            <div class="lg:w-full lg:h-48 relative ">
                                <div>
                                    <div class="line-clamp-2 my-3 lg:h-12 xl:h-16">
                                        <h3 class=" text-base lg:text-lg xl:text-2xl font-merriweather hover:text-blue">
                                            <b>{{ $post->title }}</b>
                                        </h3>
                                    </div>
                                    <div class="xl:line-clamp-4 my-3 lg:h-28">
                                        <p class="text-xs md:text-sm lg:text-base xl:text-lg w-1/2 lg:w-full ">
                                            {!! $post->description !!}</p>
                                    </div>
                                    <img
                                        src="{{ $post->image ?: 'https://s1cdn.vnecdn.net/vnexpress/restruct/i/v654/logo_default.jpg' }}"
                                        class="object-cover absolute bottom-1 right-5 lg:static w-24 h-16 lg:w-full lg:h-36 lg:mx-0 lg:my-0"
                                        alt="">
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>

            <div class="col-span-1 mx-5 hidden lg:block">
                <div class="bg-yellow w-36 h-8 py-1 my-4 text-center">MOST POPULAR</div>
                @isset($most_views)
                    @foreach ($most_views as $key => $post)
                        <div class="hover:shadow-md text-xl border-b border-dashed h-28">
                            <div class="absolute text-4xl lg:text-8xl my-auto text-lammo"> {{ $key + 1 }}</div>
                            <div class="pl-10 pt-4 my-auto relative hover:text-blue text-xs lg:text-sm xl:text-lg">
                                <a href="{{ route('web.detail', $post->slug) }}">{{ $post->title }}</a>
                            </div>
                        </div>
                    @endforeach
                @endisset

            </div>

        </div>

    </div>

@endsection
