<footer>
    <div class="lg:px-16 w-full font-roboto lg:py-4">

        <div class="relative inline-flex w-full text-center lg:px-5 lg:py-3 border-t border-b lg:border-b-0 bg-gray-300  ">

            <div class="lg:px-8 text-gray mt-12 pt-2 lg:mt-0 text-center w-full lg:w-auto absolute lg:static">
                <i class="fa-solid fa-copyright"></i><b> 2017 The Telegraph Company </b>
            </div>

            <div class="inline-flex justify-between lg:w-auto py-2">
                <div class="px-2 lg:px-3 text-sm lg:text-sm text-gray lg:w-auto"> Contact Us
                </div>
                <div class="px-2 lg:px-3 text-sm lg:text-sm text-gray lg:w-auto lg:border-l bg-gray-300">Work With Us
                </div>
                <div class="px-2 lg:px-3 text-sm lg:text-sm text-gray lg:w-auto lg:border-l bg-gray-300">Advertise
                </div>
                <div class="px-2 lg:px-3 text-sm lg:text-sm text-gray lg:w-auto lg:border-l bg-gray-300">Your Ad Choices
                </div>
                <div class="px-2 lg:px-3 text-sm lg:text-sm text-gray lg:w-auto lg:border-l bg-gray-300 hidden lg:block">Privacy
                </div>
                <div class="px-2 lg:px-3 text-sm lg:text-sm text-gray lg:w-auto lg:border-l bg-gray-300 hidden xl:block">Terms of Sale
                </div>
                <div class="px-2 lg:px-3 text-sm lg:text-sm text-gray lg:w-auto lg:border-l bg-gray-300 hidden xl:block">Site Information
                </div>
                <div class="px-2 lg:px-3 text-sm lg:text-sm text-gray lg:w-auto lg:border-l bg-gray-300 hidden xl:block">Navigation
                </div>
                <div class="px-2 lg:px-3 text-sm lg:text-sm text-gray lg:w-auto lg:border-l bg-gray-300 hidden 2xl:block">Site Map
                </div>
                <div class="px-2 lg:px-3 text-sm lg:text-sm text-gray lg:w-auto lg:border-l bg-gray-300 hidden 2xl:block">HelpSite
                </div>
                <div class="px-2 lg:px-3 text-sm lg:text-sm text-gray lg:w-auto lg:border-l bg-gray-300 hidden 2xl:block">Feedback
                </div>
                <div class="px-2 lg:px-3 text-sm lg:text-sm text-gray lg:w-auto lg:border-l bg-gray-300 hidden 2xl:block">Subscriptions
                </div>
            </div>
        </div>
    </div>
</footer>
