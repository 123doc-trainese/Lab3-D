@php use App\Models\User;use Carbon\Carbon;use Illuminate\Support\Facades\Auth; @endphp
<header class="lg:mb-4 font-roboto h-auto w-full">

    <div class="fixed top-0 inline-flex justify-between bg-[#F08080] lg:bg-[#F0F0F0] w-full
        lg:px-10 lg:border-b border-dashed shadow-2xl lg:shadow-none z-20">

        <nav class="lg:w-1/3 py-1">
            <div id="dropdown-wrapper" class="relative px-3">
                <button class="inline-block" onclick="showMenu()">
                    <i class="fa-solid fa-bars fa-2x "></i>
                </button>
                <a href="{{ route('web.home') }}"
                   class="text-2xl text-black tracking-tightest font-black hidden lg:inline-block ">
                    SECTIONS </a>
            </div>
            <!-- Dropdown menu -->
            <div class="lg:hidden">
                <div
                    class="hidden absolute left-2 bg-white text-base z-10 list-none divide-y divide-gray-100 rounded shadow my-2"
                    id="dropdown-menu">
                    <div class="px-4 py-3">
                        @if (Auth::check())
                            <button onclick="toggleMenu1()">
                                <span class="block text-sm"><i class="fa-solid fa-user mt-1"></i> Hi,
                                    {{ Auth::user()->name }}</span>
                                <span
                                    class="block text-sm font-medium text-gray-900 truncate">{{ Auth::user()->email }}</span>
                            </button>

                            <div id="menu1"
                                 class="hidden absolute px-2 -right-16 top-10 text-sm shadow-2xl bg-[#f7f5f5]">
                                <div class="py-2 hover:bg-[#F0F0F0] ">
                                    <a href="{{ route('web.profile') }}">Change Password</a>
                                </div>
                                <div class="py-2 hover:bg-[#F0F0F0]">
                                    <a href="{{ route('web.logout') }}">Login out</a>
                                </div>
                            </div>
                        @else
                            <a href="{{ route('web.login') }}"> <span
                                    class="block text-sm font-medium text-gray-900 truncate">Log in</span>
                            </a>
                        @endif
                    </div>
                    <ul class="py-1" aria-labelledby="dropdown">
                        @foreach ($categories as $key => $category)
                            <li>
                                <a href="{{ route('web.category.slug', $category->slug) }}"
                                   class="text-sm hover:bg-gray-100 text-gray-700 block px-4 py-2">*
                                    {{ $category->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </nav>

        <div class="lg:w-1/3 text-center text-gray my-auto text-base xl:text-xl">
            <p class="hidden lg:block">Friday, February 24, 2017</p>
            <p class="h-auto w-full text-center text-black text-2xl mg:py-3 lg:hidden font-bitter"><a
                    href="{{ route('web.home') }}"> <b>The News</b> </a>
            </p>
        </div>

        <div class="lg:w-1/3 text-right py-1 px-3 text-sm my-auto">
            <div class="inline-flex gap-3 xl:text-base">
                <div class="my-auto hidden lg:block">
                    <i class="fa-solid fa-magnifying-glass text-gray mx-2"></i>
                    <label for="search"></label>
                    <input id="search" name="search"
                           class="hidden px-2 focus:outline xl:w-48 lg:h-10 xl:inline-block border border-gray rounded-xl"
                           type="search" size="20" placeholder="SEARCH">
                </div>

                @if (Auth::check())
                    <div class="dropdown-notifications text-base xl:text-xl text-center relative ">
                        <nav class="inline-flex gap-1 lg:gap-4">
                            <button onclick="toggleNotify()" class="bg-gray rounded-full px-2 py-1.5 relative">
                                <i class="fa fa-bell" aria-hidden="true"></i>
                                <div data-count="0"
                                     style="{{ auth()->user()->unreadNotifications->count() > 0 ? "display: block;" : "display: none;"}}"
                                     class="bell text-white text-sm absolute -bottom-1 -right-1 bg-red rounded-full w-5">
                                    {{ auth()->user()->unreadNotifications->count() ?? 0 }}
                                </div>

                            </button>
                            {{-- drop notication --}}
                            <div id="notify"
                                 class="hidden text-xs xl:text-lg drop-shadow-2xl absolute top-10 lg:top-16 right-0 lg:-right-10 rounded-lg w-72 lg:w-96 border border-gray">
                                <div
                                    class="inline-flex w-full justify-between px-2 py-2 my-auto border-b border-gray bg-white">
                                    <p>Notifications (<span class="notify-count">
                                            {{ auth()->user()->notifications->count() }}</span>)
                                    </p>
                                </div>
                                <ul class="dropdown-menu bg-white rounded-b-lg overflow-y-auto h-52 lg:h-80">
                                    @foreach (auth()->user()->notifications->sortBy('time') as $notify)
                                        <li class="px-3 py-2">
                                            <a href="{{ route('markasread', $notify->id) }}">
                                                <div class="inline-flex gap-2 w-full">
                                                    <div>
                                                        <img
                                                            src="{{ $notify->data['avata'] ?? User::AVATA }}"
                                                            alt="" class="w-8 object-cover">
                                                    </div>
                                                    <div class="text-left w-full">
                                                        <div class="inline-flex justify-between w-full">
                                                            <b>{{ $notify->data['username'] }}</b>
                                                        </div>
                                                        <p>{{ $notify->data['content'] }}</p>
                                                        <small>{{ Carbon::parse($notify->data['time'])->diffForHumans() }}</small>
                                                    </div>
                                                    <div>
                                                        @if (!$notify->read_at)
                                                            <p class="bg-[#00ff6a] w-3 h-3 my-auto rounded-full"></p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <button onclick="toggleMenu()" class="relative hidden lg:block focus:border-none">
                                <img
                                    src="{{ Auth::user()->avata ?? User::AVATA }}"
                                    alt="" class="h-10 rounded-full object-cover">
                                <div id="action_click_down" class="">
                                    <i class="fa fa-caret-down absolute -bottom-1 right-0 text-green"
                                       aria-hidden="true"></i>
                                </div>
                                <div id="action_click_asc" class="hidden">
                                    <i class="fa fa-sort-asc absolute -bottom-3 right-0 text-green"
                                       aria-hidden="true"></i>
                                </div>
                            </button>
                            {{-- drop menu --}}
                            <div id="menu"
                                 class="hidden text-xs xl:text-lg bg-white drop-shadow-2xl absolute top-6 lg:top-16 -right-10 rounded-lg w-72 border border-gray">
                                <div class="inline-flex justify-between w-full bg-gray text-base px-3 py-1">
                                    <p><i class="fa fa-minus" aria-hidden="true"></i> Menu</p>
                                    <p><i class="fa fa-cog" aria-hidden="true"></i> All Settings</p>
                                </div>
                                <div class="inline-flex justify-between w-full px-3">
                                    <p class="w-32 my-auto text-left"> Hi, {{ Auth::user()->name }} </p>
                                    <div class="py-2 hover:text-blue">
                                        <a href="{{ route('web.logout') }}">
                                            <i class="fa fa-sign-out" aria-hidden="true"></i> Login out
                                        </a>
                                    </div>
                                </div>
                                <div class="py-2 hover:text-blue">
                                    <a href="{{ route('web.profile') }}"><i class="fa fa-wrench"
                                                                            aria-hidden="true"></i>
                                        Change Password</a>
                                </div>

                            </div>
                        </nav>
                    </div>
                @else
                    <button class="bg-sport rounded-md p-2 lg:w-24 lg:h-10 "><a href="{{ route('web.login') }}">
                            Log in</a></button>
                @endif
            </div>
        </div>
    </div>

    <div class="w-full h-auto text-center lg:text-8xl lg:px-10 lg:pt-20 lg:inline-block">
        <p class=" mt-5 text-center text-bitter hidden lg:block"><a href="{{ route('web.home') }}"> <b>The News</b>
            </a>
        </p>
    </div>

    <div
        class="text-center w-full border-t border-b bg-[#fafafa] lg:px-10 lg:my-3 lg:py-3 hidden lg:inline-block text-md">
        @foreach ($categories as $key => $category)
            <a class="mx-3 py-3 text-md xl:text-xl hover:text-blue"
               href="{{ route('web.category.slug', $category->slug) }}">{{ $category->name }}</a>
        @endforeach
        @if ($categories->count() > 0)
            <a class="mx-3 my-3 xl:text-xl bg-[#f0f0f0] hover:text-blue lg:hidden"
               href="{{ route('web.category') }}">More</a>
        @endif
    </div>
</header>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" crossorigin="anonymous"
        referrerpolicy="no-referrer"></script>

<script src="https://ajax.googleapis.com/ajax/libs/d3js/5.7.0/d3.min.js"></script>
<script src="https://js.pusher.com/7.2/pusher.min.js"></script>

<script type="text/javascript">
    //
    let notificationsWrapper = $('.dropdown-notifications');
    let notifications = notificationsWrapper.find('ul.dropdown-menu');

    let notificationsCountElem = notificationsWrapper.find('div[data-count]');
    let notificationsCount = parseInt(notificationsCountElem.data('count'));

    let notificationsW = $('.chat-action');
    let Element = notificationsW.find('div[data-count]');
    let ElementCount = parseInt(Element.data('count'));

    let notificationsCountAll = notificationsWrapper.find('.notify-count');
    let value = {{ auth()->user()? auth()->user()->notifications->count(): 0 }};

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;
    let pusher = new Pusher('ba9aa4a74e91ff60fd16', {
        cluster: 'ap1',
        encrypted: true,
        authEndpoint: '/broadcasting/auth',
        auth: {
            params: {
                id: {{ auth()->id() ?? 00000 }},
            },
            headers: {
                'X-CSRF-Token': '{{ csrf_field() }}'
            }
        },
    });

    let channel = pusher.subscribe('my-channel-{{ Auth::user()->id ?? '' }}');
    channel.bind('like-event', function (data) {
        let avata;
        if (data !== '') {

            if (data.user.avata) {
                avata = data.user.avata;
            } else {
                avata = "{{ User::AVATA }}";
            }

            let existingNotifications = notifications.html();
            let newNotificationHtml = `<li class="px-3 py-2">
                                        <a href="#">
                                            <div class="inline-flex gap-2 w-full">
                                                <div>
                                                    <img
                                                        src="` + avata + `"
                                                        alt="" class="w-8 object-cover">
                                                </div>
                                                <div class="text-left">
                                                    <div class="inline-flex justify-between w-full">
                                                        <b> ` + data.user.name + ` </b>
                                                    </div>
                                                    <p>Đã thích bình luận của bạn</p>
                                                    <small>{{ Carbon::parse(shell_exec(" + data.time + "))->diffForHumans() }}</small>
                                                </div>
                                                <div>
                                                    <p class="bg-[#00ff6a] w-3 h-3 my-auto rounded-full"></p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>`;

            notifications.html(newNotificationHtml + existingNotifications);

            notificationsCount += 1;
            notificationsCountElem.attr('data-count', notificationsCount);
            notificationsCountElem.html(notificationsCount);
            notificationsCountElem.show();

            value += 1;
            notificationsCountAll.html(value);
        }
    });

    channel.bind('add-group-chat-event', function (data) {
        let avata;
        if (data !== '') {

            if (data.user.avata) {
                avata = data.user.avata;
            } else {
                avata = "{{ User::AVATA }}";
            }

            let existingNotifications = notifications.html();
            let newNotificationHtml = `<li class="px-3 py-2">
                                        <a href="#">
                                            <div class="inline-flex gap-2 w-full">
                                                <div>
                                                    <img
                                                        src="` + avata + `"
                                                        alt="" class="w-8 object-cover">
                                                </div>
                                                <div class="text-left">
                                                    <div class="inline-flex justify-between w-full">
                                                        <b> ` + data.user.name + ` </b>
                                                    </div>
                                                    <p>Đã mời bạn <a href="#" class="hover:text-green">tham gia</a> nhóm chat</p>
                                                    <small>{{ Carbon::parse(shell_exec(" + data.time + "))->diffForHumans() }}</small>
                                                </div>
                                                <div>
                                                    <p class="bg-[#00ff6a] w-3 h-3 my-auto rounded-full"></p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>`;

            notifications.html(newNotificationHtml + existingNotifications);

            notificationsCount += 1;
            notificationsCountElem.attr('data-count', notificationsCount);
            notificationsCountElem.html(notificationsCount);
            notificationsCountElem.show();

            value += 1;
            notificationsCountAll.html(value);
        }
    });

    channel.bind('new-message', function (data) {
        let height;
        let avata;

        if (data) {
            let chat_group = $('#ul-chat-' + data.group_id).html();

            if (data.user.avata) {
                avata = data.user.avata;
            } else {
                avata = "{{ User::AVATA }}";
            }

            let html = `<li class="inline-flex w-full">
                                    <div class="my-auto">
                                        <img src="` + avata + `" alt="" class="rounded-full object-cover w-8">
                                    </div>
                                     <div class="relative max-w-xl px-4 py-2">
                                           <p class="text-xs">` + data.user.name + `</p>
                                          <span class="bg-[#F0F0F0] px-2 py-1 rounded-md text-sm">` + data.message + `</span>
                                     </div>
                                 </li>`;

            $(`#ul-chat-` + data.group_id).html(chat_group + html);

            if ($('#group-' + data.group_id).length) {
                height = $('#ul-chat-' + data.group_id)[0].scrollHeight;
            } else {
                height = null;
            }

            $('#ul-chat-' + data.group_id).scrollTop(height);

            $('#group-' + data.group_id).find('div#divcolor').removeClass('bg-[#AAAAAA]');
            $('#group-' + data.group_id).find('div#divcolor').addClass('bg-[#00008B]');

            ElementCount += 1;
            Element.attr('data-count', ElementCount);
            Element.html(ElementCount);
            Element.show();
        }
    });
</script>
