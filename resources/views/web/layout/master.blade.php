<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    @include('web.layout.style')

    @stack('style')

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <script src="{{ mix('js/app.js') }}" defer></script>
</head>

<body>
    @if (session('errors'))
        <div class="w-full text-center text-lg h-8 text-white bg-red">
            <div> {{ session('errors') }} </div>
        </div>
    @endif
    @if (session('message'))
        <div class="w-full text-center text-lg h-8 text-white bg-green">
            <div> {{ session('message') }} </div>
        </div>
    @endif
    @include('web.chat.groupchat')

    @include('web.layout.header')

    <div class="w-full">
        @yield('content')
    </div>

    @include('web.layout.footer')

    @include('web.layout.script')

</body>

</html>
