<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery@3.2/dist/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.min.js"></script>
<script src="https://unpkg.com/@themesberg/flowbite@latest/dist/flowbite.bundle.js"></script>

<script>
    // tab menu
    const tabs = document.querySelectorAll('[data-tab-target]')
    const tabContents = document.querySelectorAll('[data-tab-content]')
    tabs.forEach(tab => {
        tab.addEventListener('click', () => {
            const target = document.querySelector(tab.dataset.tabTarget)
            tabContents.forEach(tabContent => {
                tabContent.classList.remove('active')
            })
            tabs.forEach(tab => {
                tab.classList.remove('active')
            })
            tab.classList.add('active')
            target.classList.add('active')
        })
    })

    // var overlay = document.getElementById('overlay');
    var menu = document.getElementById("menu");
    var notify = document.getElementById("notify");

    function toggleNotify() {

        if (notify.classList.contains('hidden')) {
            notify.classList.remove('hidden');
            menu.classList.add('hidden');
        } else {
            notify.classList.add('hidden');
        }
    }

    // profile menu
    function toggleMenu() {
        var action_click_asc = document.getElementById("action_click_asc");
        var action_click_down = document.getElementById("action_click_down");
        if (menu.classList.contains('hidden')) {
            menu.classList.remove('hidden');
            notify.classList.add('hidden');

            action_click_asc.classList.remove('hidden');
            action_click_down.classList.add('hidden');
        } else {
            menu.classList.add('hidden');
            action_click_asc.classList.add('hidden');
            action_click_down.classList.remove('hidden');
        }
    }

    //Mobile
    // dropdown menu
    function showMenu() {
        var dropdown_menu = document.getElementById("dropdown-menu");
        if (dropdown_menu.classList.contains('hidden')) {
            dropdown_menu.classList.remove('hidden');

            menu.classList.add('hidden');
            notify.classList.add('hidden');
        } else {
            dropdown_menu.classList.add('hidden');
        }
    }

    // profile menu
    function toggleMenu1() {
        var menu1 = document.getElementById("menu1");
        if (menu1.classList.contains('hidden')) {
            menu1.classList.remove('hidden');
        } else {
            menu1.classList.add('hidden');
        }
    }

    // css
    $(document).ready(function () {
        $('#owl-theme').slick({
            prevArrow: false,
            nextArrow: false,
            respondTo: 'slider',
            autoplaySpeed: 1000,
            // autoplay: true,
            loop: true,
            slidesToShow: 3,
            responsive: [{
                breakpoint: 640,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }, {
                breakpoint: 320,
                settings: {
                    slidesToShow: 1
                }
            }]
        });

        $("figcaption").addClass('text-lg text-center');
        $("p span.location-stamp").after(' - ');
        // $("img.lazy").addClass('object-cover w-full');
        $(".Normal").addClass('my-2');
        $("ul.list-news").show();
        $("ul.list-news").addClass('list-disc text-blue underline mx-5');
        $("span.meta-news svg.ic").remove();
        $("span.icon_thumb_videophoto").remove();
    });
</script>


