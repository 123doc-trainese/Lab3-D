<?php

use App\Http\Controllers\Admin\CrawlController;
use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('user', 'UserCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('post', 'PostCrudController');
    Route::crud('crawl-site', 'CrawlSiteCrudController');

    Route::get('runcrawl/{id}', [CrawlController::class, 'runcrawl'])->name('runcrawl');
    Route::get('stopcrawl/{id}', [CrawlController::class, 'stopcrawl'])->name('stopcrawl');

    Route::post('set-time-crawl', [CrawlController::class, 'setTime'])->name('setuptime.post');

    Route::crud('config', 'ConfigCrudController');
}); // this should be the absolute last line of this file
Route::get('home', function () {
    return redirect()->route('web.home');
});
