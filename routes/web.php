<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Auth\ConfirmCreateController;
use App\Http\Controllers\Auth\FacebookController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\GoogleController;
use App\Http\Controllers\Web\Chat\ConversationController;
use App\Http\Controllers\Web\Chat\GroupController;
use App\Http\Controllers\Web\HomeController;
use App\Http\Controllers\Web\Like\LikeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Web page */
Route::get('/', [HomeController::class, 'home'])->name('web.home');
Route::get('detail/{slug}', [HomeController::class, 'detail'])->name('web.detail');
Route::get('category', [HomeController::class, 'category'])->name('web.category');
Route::get('category/{slug}', [HomeController::class, 'categorySlug'])->name('web.category.slug');

// Like event
Route::post('like', [LikeController::class, 'like'])->name('like.store');
Route::post('dislike', [LikeController::class, 'dislike'])->name('like.destroy');
Route::post('totalLikes/{comment_id}', [LikeController::class, 'countLike'])->name('like.total');
Route::get('list-liker/{comment_id}', [LikeController::class, 'getLiker'])->name('likes.getlistliker');

// Mark as Read notification
Route::get('/markasread/{id}', function ($id) {
    if ($id) {
        \auth()->user()->unreadNotifications->where('id', $id)->markAsRead();
    }
    return back();
})->name('markasread');

// Create group chat
Route::post('create-group', [GroupController::class, 'store'])->name('group.chat.store');
Route::post('get-users', [GroupController::class, 'getUsers'])->name('group.get.users');

Route::post('search-user', [GroupController::class, 'search'])->name('group.chat.search');
Route::post('save-user', [GroupController::class, 'saveUser'])->name('group.chat.search');

Route::get('leave-group-chat/{id}', [GroupController::class, 'leaveGroup'])->name('group.chat.leave');


Route::get('get-group-chat', [GroupController::class, 'getListGroup'])->name('group.chat.list');
Route::get('get-content', [ConversationController::class, 'getConversations'])->name('group.chat.get');

Route::get('read-at', [ConversationController::class, 'readAt'])->name('group.chat.read');

Route::post('send-message', [ConversationController::class, 'store'])->name('send.message.chat');

/*Forgot Password*/
Route::get('forget-password', [
    ForgotPasswordController::class,
    'showForgetPasswordForm'
])->name('forget.password.get')->middleware('check.logout');

Route::post('forget-password', [
    ForgotPasswordController::class,
    'submitForgetPasswordForm'
])->name('forget.password.post')->middleware('check.logout');

Route::get('reset-password/{token}',
    [ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get')->middleware('check.logout');

Route::post('reset-password', [
    ForgotPasswordController::class,
    'submitResetPasswordForm'
])->name('reset.password.post')->middleware('check.logout');

/*Confirm create account*/
Route::get('waitting-confirm-account', [ConfirmCreateController::class, 'waitting'])->name('web.waitting')->middleware('check.auth');
Route::post('resend-email', [ConfirmCreateController::class, 'resendEmailConfirm'])->name('resend.email')->middleware('check.auth');
Route::get('confirm-create-account/{token}', [ConfirmCreateController::class, 'submitConfirmForm'])->name('confirm.create.account.post');

// Facebook Login URL
Route::prefix('facebook')->group(function () {
    Route::get('auth', [FacebookController::class, 'loginUsingFacebook'])->name('login.facebook');
    Route::get('callback', [FacebookController::class, 'callbackFromFacebook']);
});

// Google URL
Route::prefix('google')->group(function () {
    Route::get('login', [GoogleController::class, 'loginWithGoogle'])->name('login.google');
    Route::any('callback', [GoogleController::class, 'callbackFromGoogle']);
});

/* Login , Logout , Edit Profile -> Web */

Route::prefix('web')->group(function () {

    Route::get('login', [AuthController::class, 'login'])->name('web.login')->middleware('check.logout');

    Route::post('check', [AuthController::class, 'checkLogin'])->name('web.auth.login')->middleware('check.logout');

    Route::get('register', [AuthController::class, 'register'])->name('web.register')->middleware('check.logout');

    Route::post('register', [AuthController::class, 'store'])->name('web.register.store')->middleware('check.logout');
    /**/
    Route::get('logout', [AuthController::class, 'logout'])->name('web.logout')->middleware('check.auth');

    Route::get('profile', [AuthController::class, 'profile'])->name('web.profile')->middleware('check.account');

    Route::put('profile', [AuthController::class, 'updateProfile'])->name('web.profile.update')->middleware('check.account');
});
