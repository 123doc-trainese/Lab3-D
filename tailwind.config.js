/** @type {import('tailwindcss').Config} */

module.exports = {
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
        "./resources/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        screens: {
            vs: "320px",
            sm: "640px",
            md: "800px",
            lg: "1024px",
            xl: "1280px",
            '2xl': "1536px",
            '4xl': "1800px",
        },
        colors: {
            modal: "#E6E6FA",
            gray: "#C7C7C7",
            white: "#FFFFFF",
            yellow: "#FBFF22",
            sport: "#E2E2E2",
            lammo: "#F5F5F5",
            red: "#FF0000",
            blue: "#0000EE",
            green: "#3ace52",
        },
        fontFamily: {
            roboto: ["Roboto", "sans-serif"],
            merriweather: ["Merriweather", "serif"],
            bitter: ["Bitter", "serif"],
        },
        extend: {
            spacing: {
                76: "19rem",
                88: "22rem",
                100: "25rem",
                108: "27rem",
                112: "28rem",
                120: "30rem",
                128: "32rem",
                144: "36rem",
                148: "37rem",
                160: "40rem",
                168: "42rem",
                184: "46rem",
                256: "64rem",
                276: "69rem",
            },
            fontWeight: {
                extrablack: 1200,
            },

            letterSpacing: {
                tightest: "-.075em",
            },

            borderRadius: {
                "4xl": "2rem",
            },
        },
    },
    plugins: [
        require("@tailwindcss/line-clamp"),
    ],
};
